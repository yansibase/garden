## 医学专业教材

### 人民卫生出版社
![](img/医学教材-人民卫生出版社.jpg)
source: https://www.zhihu.com/question/495857693
(配套习题和答案：《X学学习指导与习题集》)



## [有什么书籍推荐给医学生看？](https://www.zhihu.com/question/24346913)

- 人卫《内科学》、《诊断学》
- [国内名院、名科、知名专家 临床诊疗思维系列丛书]:  
	- 《消化内科疾病临床诊疗思维》（钱家鸣），libgen
	- 《普通外科疾病临床诊疗思维》（姜洪池）,no-ebook
	- 《肾内科疾病临床诊疗思维》（陈江华),no-ebook
	- 《心血管疾病临床诊疗思维》 廖玉华,no-ebook
	- 《呼吸内科疾病临床诊疗思维》康健，libgen
- 《内科症状鉴别诊断学》张树基，罗明绮，libgen
- 《心电图图解速成讲授》王建华
- 《临床心电图解析》或《波德瑞德临床心电图解析》(Podrid's Real-World ECGs), 共六册，Podrid，郭继鸿 译,no-ebook
-  武汉亚洲心脏病医院编写：
	- 《心血管医生学影像》马小静、,no-ebook
	- 《心血管疾病影像图谱》马小静、no-ebook
	- 《结构性心脏病介入影像图谱》陈险峰、no-ebook
	- 《心血管疾病CT扫描技术》陈险峰、no-ebook
	- 《心血管影像解剖图谱》马小静、,no-ebook
- 夜班值班三件套:
	- 《急诊内科手册》（3ed.2021）张文武,（2ed jiumodiary）
		（《急诊内科学》4ed.2017 是否能代替？）
	- 《北京协和医院 内科住院医师手册》(2ed2014)赵久良、(2ed2021)李骥.  
	- 《常见病处方速查》袁洪,no-ebook！！！
- 《实用外科医嘱手册(第2版) 》梁力建,胡文杰,陈伟 主编   
		
北京协和医院及各大医学院校的住院医师手册系列！简直是见习+实习节段轮转的法宝！
![](img/住院医师手册系列.webp)

- 《协和内科住院医师手册（第二版）》赵久良、冯云路
- 《协和内科住院医师手册（第三版）》施文（协和出版社的微店目前有卖） ,no-ebook！！！
- 《协和临床用药速查手册》韩潇 ,no-ebook！！！
    (《临床用药速查手册》,苏冠华, 可否代替？)
![](img/住院医师手册系列-内科.webp)

- 《妇产科手册》（人民卫生出版社）郑勤田
- 《神经科手册》（人民卫生出版社）贾建平,  no-ebook！！！
- 《儿科住院医师手册》（中国协和医科大学出版社）李仲智，no-ebook！！！
![](img/住院医师手册系列-妇儿神内.webp)

- 《协和临床外科手册》于健春,  no-ebook！！！
- 《协和 外科住院医师手册》徐协群，b-ok
- 《实用外科医嘱手册》（2ed）梁力建
![](img/住院医师手册系列-外科.webp)

- 《风湿免疫科医师效率手册》唐福林
- 《北京协和医院血液科诊疗常规》（内部资料），no-ebook
![](img/住院医师手册系列-风湿血液.webp)

（2022新协和4本：  
	- 协和内科住院医师手册(3ed)，施文，
	- 实用外科医嘱手册(2ed)，梁力建，
	- 临床用药速查手册，韩萧，
	- 协和临床外科手册，于健春）


- 实用内科学(15ed林果为)(16ed.2022.王吉耀), libgen
![](img/实用内科学.webp)  
- 哈利森感染病学(Harrison's Infectious Diseases)3ed.2017.：  (libgen 英文版)
![](img/住院医师手册系列-实用内科学哈利森感染病学.webp)  


吴在望-全系类思维导图图书:
![](img/吴在望系列1.webp)  
![](img/吴在望系列2.webp)  

研大的思维导图。  

Molecular Biology Of The Gene(7ed.2013.Watson)(基因的分子生物学2015) ：（libgen）
![](img/MolecularBiologyOfTheGene.webp)  

精神障碍诊断与统计手册(DSM-5)：(libgen)
![](img/DSM-5.webp)  

The Science Of Orgasm(Barry R. Komisaruk)性高潮的科学，胡佩诚：(libgen 英文版)
![](img/TheScienceOfOrgasm.webp)  

王忠诚神经外科学，王忠诚：libgen
![](img/王忠诚神经外科学.webp)  



实用外科学（上下）4ed-吴肇汉：libgen
![](img/实用外科学.webp)  

实用神经病学.4ed.吕传真：no-ebook！！！
![](img/实用神经病学.webp)  

积水潭 实用骨科学-田伟：
![](img/实用骨科学.webp)  

Williams Textbook Of Endocrinology.HM Kronenberg（威廉姆斯内分泌学.向红丁）：libgen
![](img/WilliamsTextbookOfEndocrinology.webp)  

Netter's Atlas Of Neuroscience奈特 人体神经解剖彩色图谱，David L. Felten，崔益群译：b-ok.cc
![](img/Netter'sAtlasOfNeuroscience.webp)  

临床神经解剖学 2ed，芮德源：libgen
![](img/临床神经解剖学.webp)  

沈渔邨精神病学 6ed，陆林：5ed on libgen
![](img/沈渔邨精神病学.webp)  

Principles Of Neurobiology.LiqunLuo（神经生物学原理.骆利群）：no-ebook  ！！！
![](img/PrinciplesOfNeurobiology.webp)  

From Neuron To Brain.5ed.John.Nicholls（神经生物学-从神经元到脑-杨雄里）：libgen
![](img/FromNeuronToBrain.webp)  

实用疼痛学，刘延青：no-ebook ！！！  
![](img/实用疼痛学.webp)  

the Biology Of Cancer.2ed.R.A.Weinberg（癌生物学.詹启敏）：(中文1ed,b-ok.cc。英文 2ed2014，libgen）
![](img/theBiologyOfCancer.webp)  

Human Molecular Genetics.T.斯特罗恩（人类分子遗传学.孙开来） ：
![](img/HumanMolecularGenetics.webp)  





https://www.zhihu.com/question/41383581/answer/91198571  
学医的，前两年学的基本一样，解剖、生理、生化、组织胚胎学。后来再加免疫，细胞生物学、病理、药理，然后才是专业课。
**假如**2016年的职业医师考试综合，**侧重点**是内科的泌尿，
你手里却是一本2015年的考试综合，里面的重点是内科的**呼吸**系统。
（还有这事儿？是的，确实有）可2015年已经考过呼吸了呀，16年呼吸系统的内容考的必定就少，对吧？你还把重点放在呼吸系统上呢，你说你四不四傻？四不四有病？又有人问了，你怎么知道2016年的重点是内科的泌尿系统，没错！就在书的前几页上写着呢....对比一下，每一版的书都不一样，肯定根据要传授的内容，做了删减。删减了什么？你知道吗？没错，书的前几页会有一个详细的介绍。

九大系统：运动系统、消化系统、呼吸系统、泌尿系统、生殖系统、内分泌系统、免疫系统、神经系统、循环系统。


考证的

