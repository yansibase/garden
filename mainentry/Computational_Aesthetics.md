
_Computational Aesthetics_  
2019  
by Yasuhiro Suzuki  

## Computation and algorithms are classified into two categories:  
- machine computation (is essentially mathematics)  
the (computing) subjects(humans), computers existing outside the (computing) subject. computation is completed by the computers; the (computing) subjects are simply waiting for the computers to provide the result.  
- natural computation (is essentially aesthetics — states of beauty)  
the (computing) subjects and computers are integrated. the (computing) subjects must decide when to start and halt computation. Consequently, computations sometimes do not halt. algorithms that can lead the computing subjects to halt computation.

## Chapter 1  
Computational Aesthetics, Yasuhiro Suzuki  
Algorithm is a method of describing the sequences of processing something;  
programming is (re-)arrangements of the sequences;  

Almost all the processes in Nature or daily life are not dis-ordered but well-ordered, and the order has been called Algorithm;  

Algorithm + Executer = Computing  
in order to compute something, it requires “someone”, and “something” to execute the algorithm.  
“someone”: main constituent of computation(MCC)  
“something”: executer  

Internal MCC: in this case MCC are executers so MCC have to decide when computations halt themselves; cooking, drawing correspond to this case (Fig. 1.2),  
External MCC: in this case MCC exist in the outside of executers, so once computations start, MCC wait for the computation halt (Fig. 1.1).  

Type I: a method to perceive the world; broadly applicable and which is not limited only by using computers; not only Algebra but also cooking, teaching, brushing teeth, etc. are based on this type of algorithm;  
Type II: algorithms for computers, it is being noticed that Type I and Type II are not completely divided, Type I will give birth of Type II, vice versa; we perceive something and in some case, try to describe by using Type I and transform into Type II then observe computing in the executer such as computer simulations; and the simulations give hints to explore novel Type I algorithms.

Type I will be transformed into Type II much more rapidly than ever, so everyone will be able to obtain various techniques to create art works very easily that have been permitted to treat only well talented and trained artists.

in Type I algorithm, goodness, comfortableness, beautifulness, etc. such matters of aesthetics decide the condition of halting computing.

aesthetics is principally irreducible for algorithm of Type I and one of the most important characteristics, the condition of halt computing relies on the aesthetics of a MCC, even if we do not consider about art or culture.

Aesthetics is not only for wild life, if a protein molecular does not have a kind of tactile sense, proteins will not be able to produce functions; they have a kind of tactile sense and when a protein touches to another protein, if it is tactually “good”, they will connect with each other, otherwise does not connect; protein’s Aesthetics of tactile sense produces preferences of binding proteins and the preferences produce functions of proteins.

These results show that interesting images are output in early generation whose fitness is low or in middle generation whose fitness starts to increase. Therefore, the model that simply evolves and aims at the target image is inappropriate. This indicates that the model that sustains the fitness in early and middle generations can output better images.

These results suggest that when the fitness in this model is employed, the area where we can expect an image having high aesthetic evaluation, originality, and heuristic output

the sequence of hand movements –> the tactile sense

three main basic elements of massage: pressure, contact area, and rhythm

As a founder of Natural Computing, Leibniz considered, Algorithm is a method of considering nature. And in the Natural Computing, Aesthetics is the foundation.  

## Chapter 3  
Herder on “Sentio, Ergo Sum”: Seen from His Remarks on the Color Harpsichord, Takashi Sugiyama  
We generally consider ourselves as having five senses, among which the sense of sight and hearing count as “superior,” as opposed to the “inferior” smell, taste, and touch.

Plato: “Have the sight and hearing of men any truth in them […]? And yet if these two physical senses are not accurate or exact, the rest are not likely to be, for they are inferior to these” (Phaedo, 65B).  

## Chapter 6  
Think of Everything with Back Calculation: Computational Aesthetics of Hair Design, Shinya Tsuchiya  
Hair design can only cut hair in principle, so in this regard, it is similar to carving.  

——————————–  
Computational-Approaches-in-the-Transfer-of-Aesthetic-Values-from-Paintings-to-Photographs, by Xiaoyan Zhang, 2018  
——————————–  

how some of the aesthetic attributes of a painting may be transferred to a photograph using the latest computational approaches.

### six structural contrasts that are the foundational core of our research:  
- global contrast,  
- local contrast,  
- centre-corner contrast,  
- regional contrast,  
- neighbouring regional contrast  
- depth-aware contrast.