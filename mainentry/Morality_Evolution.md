

# The Science of Sin_ The Psychology of the Seven Deadlies (2012)

Pride, greed, sloth, gluttony, lust, envy, and anger,
 In psychology, pride, lust, gluttony, greed, envy, sloth, and anger aren’t considered “sins,” or morally wrong, or even uniformly bad, but rather are complex and largely functional psychological states.
experimental social psychologist
I want to convince you that not only are the sins complex and interesting psychological states, but that, if indulged wisely, they are also largely functional and adaptive. 

 
# Neurobiology and the Development of Human Morality_ Evolution, Culture, and Wisdom(2014), by Darcia Narvaez   

In this pioneering volume, Darcia Narvaez applies an interpersonal neurobiological lens to a fundamental problem of the human experience: morality. 
Narvaez operationally defines morality and ethical behavior as “how we relate to others, including how we share the benefits and burdens of living as social creatures,” 


# The Evolution of Ethics_ Human Sociality and the Emergence of Ethical Mindedness (2015)

Evolutionary science helps us to see how fully psychologically realistic Aristotle’s ethics is and to fill out and extend his thought.
The central point of Aristotle’s ethics is that the aim of life is to be an excellent human being, a theory of natural ethics that truly integrates the science of evolution with Aristotle’s ethical genius.



# Evolution and Ethics_ A Critique of Sociobiology (2015)

Given the long history humanity has of acting on the basis of abstract metaphysical theories (avowedly religious faiths included), and the countless disasters, massacres, wars and genocides, purges, and so on that have resulted, it is surely not polemical to raise the issue: perhaps we should try to stop ourselves from acting on metaphysics anymore, and to do this in a most sensible way, by trying to gain broader perspectives, which mustinclude critiquing specific examples of preeminent, dominant, metaphysical worldviews like Darwinism.  
human behavior can be so bad, and all the other instinctive animals so clearly not inclined to such
stupidity, that human reason must surely be the cognitive source that manifests itself in the metaphysical theories that are believed in and acted upon.
On Darwin’s theory, life evolves on the basis of come-by-chance, physical, and identifiable laws of nature. Evolution is reducible to the laws of chemistry and physics, as are each and every phenomenon, without exception.


# Commonsense Darwinism _ Evolution, Morality, and the Human Condition (2015)

Does evolutionary biology require us to give up the notion of objective moral truth?   
Does evolutionary biology require us to reject a libertarian conception of freedom?   
Does it require us to give up a correspondence theory of truth?   
My answers to these questions are: No, No, and No. This book is intended to support such answers. 
what evolutionary biology might mean for the traditional problems of philosophy.  
the philosophical implications of evolutionary biology   
I also believe that both the physical and behavioral characteristics of human beings have been shaped in significant ways by evolution. Thus, I take seriously the suggestion of sociobiologists that there is a plausible Darwinian explanation for the existence of a natural altruistic impulse in human beings.  
I also believe that there are objective moral truths that we can discover through reason and argumentation.I have long felt that an Aristotelian approach to ethics is our best hope of providing a defensible objectivist ethic, and over time I have come to the conclusion that the Darwinian theory of evolution can play an important supporting role in the defense of such an objectivist ethical theory.  
CH5 moral status of nonhuman animals. I also argue that Darwinism doesn't undermines the design argument for God’s existence.  
CH7 psychological egoism(e.g. Psychological hedonism)--humans are fundamentally selfish
CH8 freedom of the will
I think there are objective moral truths that we can discover through reason and reflection, that we have the kind of freedom that can make sense of the concept of moral responsibility, that not everything we do is done only to promote our own interests, and that eating meat is morally acceptable. I also believe that we can know things, that truth involves a correspondence between beliefs and the world, that God’s existence is a real possibility, and that Darwinism does nothing to undermine this belief.  
this book can be viewed as one philosopher’s attempt at defending such commonsense views through examining their fit with the facts of evolutionary biology.  


# Why We Need the Humanities_ Life Science, Law and the Common Good (2016)

We actually need the humanities because of their practical importance in helping policy-makers address issues directly affecting our civil liberties and significant parts of our economies. There are certainly a variety of excellent reasons for pursuing the humanities, most of which have nothing to do with economic growth;


# The origins of fairness _ how evolution explains our moral nature (2016)

Does it turn out, nonetheless, that the origin of this phenomenon actually can be explained within the framework of the natural sciences? That is the question that I set out to answer in this book. In doing so I will draw on two philosophical traditions: naturalism and contractualism.Neither of these theories is new, but in the past they have never been defended together as a unit. As we will see, however, while neither of them is
entirely convincing on its own, in combination they offer a coherent theory of morality that is backed by a wealth of evidence.



# Evolution, Explanation, Ethics and Aesthetics. Towards a Philosophy of Biology (2016)

Philosophy of Biology

“Aesthetics” is a term forged by Alexander Gottlieb Baumgarten (1714e1762)  
to refer to a philosophy of taste and beauty An important difference between ethics and aesthetics is that ethical values are considered, by and large, “objective,” while aesthetic values are considered much more “subjective.”


# The Evolution of Morality (2016)， by Todd Shackelford

conference on “The Evolution of Morality” in 2014

“Morality as Cooperation: A Problem-Centred Approach,” Oliver Scott Curry presents a new theory of morality as cooperation. This theory uses the mathematics of cooperation to identify the many distinct problems of cooperation and their solutions; and it predicts that it is the solutions deployed by humans that constitute “morality.”

Katie Hall and Sarah F. Brosnan argue in “A Comparative Perspective on the Evolution of Moral Behavior” that humans are not alone in the animal kingdom in displaying moral behavior.

Helping Another in Distress: Lessons from Rats,” Mason reviews her own and others’ research indicating that pro-social behavior occurs in rodents as well as in nonhuman primates, refl ecting the value of social cohesion and affective communication to mammals of all ages and both sexes...

Yael Sela and colleagues address religiously motivated violence as a downstream consequence of processes of sexual selection... relying on religion as the basis of one’s morality can be problematic...religiously motivated violence may have provided ancestrally within a sexual selection framework.  
religion is considered by many practitioners to form the foundation of morality. However, religiosity varies substantially at the individual and societal level. According to Liddle, understanding this variation from an evolutionary perspective can aid in disentangling religion and morality....the extent to which one feels safe walking around one’s neighborhood at night predicts religiosity,  

The Evolved Functions of Procedural Fairness: An Adaptation for Politics,” people are not only concerned about outcomes . They also readily produce moral  
evaluations of the political processes that shape these outcomes. In short, the authors argue that people have a sense of procedural fairness.  

The authors present an analysis of the Nash Equilibria of a series of simple games to reframe and explain many puzzling aspects of human morality. Hoffman and colleagues suggest that their own arguments question  
 -  the notion that morality can be justifi ed based on any a priori logic, at least one that does not account for individual incentives within one’s lifetime.   
 - the notion of “moral truths” other than if such truths are the moral intuitions that emerge from Nash Equilibria. These premises underlie much of moral philosophy and thus lead the authors to question 
 - the methodology commonly employed within the field, which often relies on psychological explanations that are little more than folk intuitions, neuropsychological description, or on unverifi able evolutionary processes with superfluous predictions.   

Hoffman and colleagues contend that their argument applies not only to esoteric philosophical debates but should also make us doubt the logic we give for our own morality, such as when we have political debates with our friends. And, it similarly draws into question the premise that moral progress is driven by reason.


# A Natural History of Human Morality, by Michael Tomasello, 2016

A Natural History of Human Thinking.2014
A Natural History of Human Morality

CH1  
two-step sequence in the evolution of human social life:  
- new forms of collaborative activity,   
- new forms of cultural organization  

these new forms of social life --> species-unique kinds of thinking  
these new forms of social life --(how to structure)--> the way that early humans came to engage in moral acts  


The uniquely human version of cooperation known as morality:  
- one may sacrifice to help another based on such self-immolating motives as compassion, concern, and benevolence  
- one may seek a way for all to benefit in a more balanced manner based on such impartial motives as fairness, equity, and justice  

classical:  
a motive for beneficence (the good)  v.s.  a motive for justice (the right)  

modern:  
a morality of sympathy v.s. a morality of fairness  


sympathy(most basic and straightforward)  
sympathy is pure cooperation,   
fairness is a kind of cooperativization of competition in which individuals seek balanced solutions to the many and conflicting demands of multiple participants’ various motives.  


Assumptions:  
- human morality is a form of cooperation.  
- human morality comprises the key set of mechanisms—psychological processes of cognition, social interaction, and self-regulation—that enable human individuals to survive and thrive in their especially cooperative social arrangements.  


Attempt in this book is:     
(1) to specify in as much detail as possible, based mainly on experimental research, how the cooperation of humans differs from that of their nearest primate relatives;  
(2) to construct a plausible evolutionary scenario for how such uniquely human cooperation gave rise to human morality  


human v.s. great ape  
The interdependence hypothesis: 1. collaboration and 2. culture  
contemporary human beings are under the sway of at least 3 distinct moralities.  
These 3 moralities are:  
- the cooperative proclivities organized around a special sympathy for kin and friends (e.g. the first person I save from a burning shelter is my child )  
- a joint morality of collaboration in which I have specific responsibilities to specific individuals in specific circumstances (e.g. the next person I save is the firefighting partner with whom I am currently collaborating (and with whom I have a joint commitment) to extinguish the fire.)  
- a more impersonal collective morality of cultural norms and institutions in which all members of the cultural group are equally valuable (e.g. I save from the calamity all other groupmates equally and impartially)  

Conflicts among these 3 moralities , e.g. should I steal the drug to save my friend?   
The bare fact of such unsolvable incompatibilities suggests a complex and not totally uniform natural history in which different cooperative challenges have been met in different ways at different times.  


the skills and motivation to construct with others an interdependent are what propelled the human species from strategic cooperation to genuine morality  

On the individual level, each partner had her own role to play in a particular collaborative activity (e.g., hunting antelopes), and over time there developed a common-ground understanding of the ideal way that each role had to be played for joint success. These common-ground role ideals may be thought of as the original socially shared normative standards. These ideal standards were impartial in the sense that that they specified what either partner, whichever of us that might be, must do in the role. Recognizing the impartiality of role standards meant recognizing that self and other were of equivalent status and importance in the collaborative enterprise.  

This novel form of moral psychology was not based on the strategic avoidance of punishment or reputational attacks from “they” but, rather, on a genuine attempt to behave virtuously in accordance with our “we.”  

modern human groups became large -->split into tribal-level groups (cultures) --> cultures competed with each other for resources → Members of a cultural group thus felt sympathy and loyalty  
To coordinate group activities → cultural conventions and norms based on cultural common ground.  

Moral norms were considered legitimate because:  
- the individual identified with the culture and so assumed a kind of coauthorship of them   
- the individual felt that her equally deserving cultural compatriots deserved her cooperation.   

Thus, members of cultural groups  felt an obligation to both follow and enforce social norms as part of their moral identity.  

participation in cultural life created a second novel form of moral psychology----cultural and group-minded, “objective” morality.  



# Why Do They Vote That Way_ From The Righteous Mind (2018), by Jonathan Haidt

The Righteous Mind: Why Good People are Divided by Politics and Religion, from which this book is excerpted.

I had developed a theory, called “moral foundations theory,” to explain why people in different societies and different social classes often disagree about what was right and wrong.

Two key principles of the core of Moral Foundations Theory:  
 - Intuitions come first, strategic reasoning comes second.  
 - The moral mind is like a tongue with six taste receptors.  

The key to persuasion is to speak to the elephant first.   

there are five strong candidates for being the basic and universal foundations of human morality:   
- 1) Care/harm: This foundation is related to our long evolution as mammals with attachment systems and an ability to feel (and dislike) the pain of others. It underlies virtues of kindness, gentleness, and nurturance.
- 2) Fairness/cheating: This foundation is related to the evolutionary process of “reciprocal altruism” described by the biologist Robert Trivers in 1971. People are strongly motivated to play “tit for tat.” This foundation generates ideas of justice, rights, and autonomy.
- 3) Loyalty/betrayal: This foundation is related to our long history as tribal creatures able to form shifting coalitions. It underlies virtues of patriotism and self-sacrifice for the group.
- 4) Authority/subversion: This foundation was shaped by our long primate history of hierarchical social interactions. It underlies virtues of leadership and followership, including deference to legitimate authority, respect for traditions, and a valuation of order or stability.
- 5) Sanctity/degradation: This foundation was shaped by the psychology of disgust and contamination. It underlies religious notions of striving to live in an elevated, less carnal, more noble way. It underlies the widespread idea that the body is a temple which can be desecrated by immoral activities and contaminants.



# Evolution Science and Ethics in the Third Millennium, Challenges and Choices for Humankind, by Robert Cliquet, Dragana Avramov, 2018

This book aims to revitalise and substantiate the idea that evolution science can  provide a rational and robust framework for elaborating the main guidelines of a universal morality for the future. It also traces pathways for long-term biological evolution and cultural development and adaptation of humanity, in the direction of ecological sustainability of our planetary environment....most secular ideologies only deal with humanity’s present and future in a rather fragmented way and with a short-term perspective. Hence, the need for *another framework to rethink values and norms* that would safely guide the human species in making choices throughout new subsequent stages of biological evolution and cultural development (at the planetary level).

to use evolution science(bio-anthropology) to study the biological origin, present and future of humanity.  

rationale: the interaction of the long-term hominisation process with the fast changing environment of modernisation.  

ethical aspects discussed:  
individual variation,  
inter-personal variation,   
inter-group variation  
inter-generational variation.  

Evolution-Based Universal Morality

CH1
two major developmental processes:  
 - the hominisation process   
 - the modernisation process,  

It is argued that the evolutionary approach is a good way to bring together religious and secular ideologies in order to reflect upon and develop a new global ethics focussed on a long-term evolutionary perspective. It systematises the questions that need to be addressed regarding the values and norms to be adapted and shaped for the future successful enhancement of human potentialities conducive to further hominisation.


# Conscience_ The Origins of Moral Intuition (2019), by Patricia S. Churchland

conscience is an individual’s judgment about what is morally right or wrong, typically, but not always, reflecting some standard of a group to which the individual feels attached.   

conscience is judged by:   
- feelings that urge us in a general direction,   
- judgment that shapes the urge into a specific action.  

Conscience's social dimension: knowledge of community standards.   

Can science tell us, for any particular moral dilemma, what option our conscience should take—in other words, which is the morally right choice? No.Factual evidence can, however, assist with the decision.
Can science tell us how it is that we have a conscience at all? I think the answer is yes.

We are social by nature.   
what biological evidence supports the idea that humans are social by nature? Research in neuroscience has improved our understanding of what it is about wiring in mammalian brains, including human brains, that makes us social. Very roughly, our infant brains are genetically set up to take pleasure in the company of certain others and to find separation from them painful. We are attached to mothers and fathers, to siblings and cousins and grandparents. As we mature, we become attached to friends and mates. These attachments are a profoundly important source of meaning in our lives, and they motivate a wide range of social behavior.  
neuroscience has begun to understand [what learning social skills and habits] entails that change as we learn, and the genes that respond to make those brain changes permanent.   

distinctive personality --> distinctive conscience  
“How should we evaluate the facts to make the right decision?”. whether to trust the information on the table. 

how brains acquire values, including moral values, and how moral values guide decisions.   
If we think of conscience as involving the internalization of community standards, then:  
 - how this internalization is processed  
 - how it happens that a community standard can change, or how an individual can come to regard a certain accepted social practice as immoral.  

Although neuroscience can help explain aggression in parents who defend their young against predators, aggression by one social group against members of another group is poorly understood at the level of the brain.   

Psycholinguists have shown that our everyday concepts such as vegetables have a radial structure: the core of the concept are examples that everyone agrees fall under the concept, but then encircling the core are examples that are somewhat similar to those in the core, but that not everyone agrees belong under the concept.  

how neurons and neural networks integrate information from various sources to make a decision.These discoveries turn out to be relevant to moral decision-making, and they yield a deeper understanding of the nature of morality in human societies.  



# Blueprint: The Evolutionary Origins of a Good Society (2019), by Nicholas A. Christakis

Preface:  
Natural selection has equipped us with the capacity and desire to join groups, and to do so in particular ways. For instance, we can surrender our own individuality and feel so aligned with a collective that we do things that would seem against our personal interests or that would otherwise shock us.

“go mad in herds, while they only recover their senses slowly, and one by one.”...deindividuation: people begin to lose their self-awareness and sense of individual agency as they identify more strongly with the group, which often leads to antisocial behaviors they would never consider if they were acting alone. 类比复杂网络？类比神经元与神经网络？  

As with the capacity for empathy, the inclination to assemble into groups and deliberately choose friends and associates is part of our species’ universal heritage.

My vision of us as human beings, which lies at the center of this book, holds that people are, and should be, united by our common humanity. And this commonality originates in our shared evolution. It is written in our genes. Precisely for this reason, I believe we can achieve a mutual understanding among ourselves.

This fundamental claim about our common humanity has deep philosophical roots as well as empirical foundations.  
In my view, recognizing this common humanity makes it possible for all of us to lead grander and more virtuous lives.  

Where does this cross-cultural similarity come from? How can people be so different from—even go to war with—one another and yet also be so similar? The fundamental reason is that we each carry within us an evolutionary blueprint for making a good society.  

CH12  
We could say that a society is good when it enhances its members’ happiness or survival  

Reviews:  
bridging the conceptual chasm between the choices of individual people and the shaping of an entire society. how the better angels of our nature, rooted in our evolutionary past, can bring forth an enlightened and compassionate civilization.”...... leadership, friendship, and group tendencies are all rooted in the most fundamental mechanism of our biological sorting: natural selection.......all human cultures converge on a consistent style of social network, and in Blueprint he explores the reasons why. gene-based account does not have to challenge the impact of culture, nor does it commit the analysis to reductionism or determinism.......goodness has a biological purpose. kindness and love are not merely things we can do—but things we must do.......Tribalism is all around us, but it does not have to be. ......the core of our nature—this obligatory patterning of ourselves into units called society, with the building blocks being love, friendship, cooperation, and learning. ......Darwin still applies: the survival of the fittest may mean the survival of those among us who can see beyond themselves and work with others doing the same. And therein lies some real cause for optimism.......It brings to bear a long history of research to show that cooperation and prosocial traits of humans are genetically based and are the result of evolution by natural selection. By doing this, Christakis corrects one the most frequent misperceptions about biological evolution, namely that inter-individual competition is a law of nature. 

