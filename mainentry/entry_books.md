## Cosmos and Culture - Cultural Evolution in a Cosmic Context  

Introduction   

Part 1: The Cosmic Context    
1. [Cosmic_Evolution_-_State_of_the_Science](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/Cosmic_Evolution_-_State_of_the_Science.md)   
2. Cosmic Evolution - History, Culture, and Human Destiny, (Steven J. Dick)    

Part 2: Cultural Evolution    
3. [Social_Evolution_-_State_of_the_Field](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/Social_Evolution_-_State_of_the_Field.md)   
4. [The_Evolution_of_Culture](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/The_Evolution_of_Culture.md)   
5. [The_Big_Burp_and_the_Multiplanetary_Mandate](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/The_Big_Burp_and_the_Multiplanetary_Mandate.md)   
6. [Evo_Devo_Universe_-_A_Framework_for_Speculations_on_Cosmic_Culture](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/Evo_Devo_Universe_-_A_Framework_for_Speculations_on_Cosmic_Culture.md)   
7. [Dangerous_Memes](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/Dangerous_Memes.md)   

Part 3: Cosmos and Culture  
8. [Cosmocultural_Evolution](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/Cosmocultural_Evolution.md)   
9. [The_Intelligent_Universe](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/The_Intelligent_Universe.md)   
10. [Life_Mind_and_Culture](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/Life_Mind_and_Culture.md)   
11. [The_Value_of_L_and_the_Cosmic_Bottleneck](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/The_Value_of_L_and_the_Cosmic_Bottleneck.md)   
12. [Encoding_Our_Origins](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/Encoding_Our_Origins.md)   
13. [History_and_Science_after_the_Chronometric_Revolution](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/History_and_Science_after_the_Chronometric_Revolution.md)   
14. [Bringing_Culture_to_Cosmos](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/Bringing_Culture_to_Cosmos.md) （综述）  
15. [Bringing Cosmos to Culture - Harlow Shapley and the Uses of Cosmic Evolution](Cosmos_and_Culture_-_Cultural_Evolution_in_a_Cosmic_Context/Bringing_Cosmos_to_Culture.md) (JoAnn Palmeri)  
















