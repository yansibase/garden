## The Importance of Living
by Lin Yutang  

## My Country and My People
by Lin Yutang  

## Wang xiaobo’s essays (Silent Majority, My Spiritual Home, A Unique Pig, …)  
In my opinion, these are the very readings to cultivate and reflect Chinese value.  

## The Deep Structure of Chinese Culture
by Sun Longji  
This book gives a reasonable description of Chinese thought.    

## The Geography of Thought
The chapter 2 of this book gives several possible factors that cultivate the Chinese thought in ancient times —- the origin of the Chinese thought.   

## Shen Mei Yu Ren De Zi You
by Liu xiaobo  
This dissertation addresses a question of why people need Aesthetics besides Sciences and Religions.    