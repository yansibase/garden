
## 术语
temes:  technological memes  


##  
Pandora: humans  
Opening Box: let loose cultural evolution  
danger points: 
- capable of behavioral imitation;  
- coevolve towards a more symbiotic relationship - a stable mutualism;  



[[Evo_Devo_Universe_-_A_Framework_for_Speculations_on_Cosmic_Culture#Evolutionary Developmental Processes of X | Evolutionary Developmental Processes]] of temes:  copying, varying, and selecting information

Given system A and B:  
- system A that copies the instructions for making a product. (copy information)   
- system B that copies the product itself.  (copy physical material (and information?))  
A is better than B.    

technological evolution shift:  
copy-the-product --> copy-the-instructions for making a product

### on extraterrestrial intelligence
intelligence is a product of replicator power. it is a second-level replicator(not intelligence) that forces a
great leap forward in living creatures or creates the possibility of culture. This second replicator creates an environment in which greater intelligence is adaptive. Intelligence therefore increases, helping to provide a situation in which a third-level of replicator can arise. This third level entails the creation of replicating machinery outside of any original Pandoran species, a crucial step towards interplanetary communication, and so towards the possibility of others communicating with us.   
之所以到3rd-level intelligence为止，是因为作者借用[Drake equation](https://en.wikipedia.org/wiki/Drake_equation#Equation): $N = n × f_{R1} × f_{R2} × f_{R3} × L$

### selfish replicator
replicator's ability to multiply whenever conditions are right, and don't care the effect on anything else. 

memes和temes之所以危险，是因为它们内在的"selfish"属性。  

the increase in human brain size ... was meme driven and therefore potentially dangerous.  



R1, R2 machines --> R2, R3 machines --> R3, R4 machines --> R4, R5 machines --> ....

R1: DNA,  
R2: memes   
R3: temes   
R4: An R3 level culture might develop the machinery to copy itself and so to seed new variants of that culture on different planets. This would lead
to competition between variant cultures, and the evolution across the cosmos of R4 level civilizations. If this happened, planets separated by large distances
would play a role analogous to islands here on Earth—creating relatively isolated conditions in which cultures would evolve in different directions.
 

R2/memes machines: humans, (any living things (that can give birth to next generation) ?)   
R3/teme machines: writing, printing, communications systems,  broadcasting, sound&image recording systems, behavioral traditions, education, symbolic culture, tools, and buildings.   


the direction of cultural evolution will depend on:  
- the kind of species in which that culture first emerged,   
- how much scope there is for transforming that species into an efficient next-level-replicator machine,   
- whether the species has limbs suitable for constructing rich material culture,   
- what material resources there are available for such construction,   

