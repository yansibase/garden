# The Value of “L” and the Cosmic Bottleneck

Seth Shostak

## [Drake Equation - Wikipedia](https://en.wikipedia.org/wiki/Drake_equation)

${\displaystyle N=R_{*}\cdot f_{\mathrm {p} }\cdot n_{\mathrm {e} }\cdot f_{\mathrm {l} }\cdot f_{\mathrm {i} }\cdot f_{\mathrm {c} }\cdot L}$  
where  
$N$ = the number of civilizations in the Milky Way galaxy with which communication might be possible (i.e. which are on the current past light cone);  
$R_∗$ = the average rate of star formation in our Galaxy  
$f_p$ = the fraction of those stars that have planets  
$n_e$ = the average number of planets that can potentially support life per star that has planets  
$f_l$ = the fraction of planets that could support life that actually develop life at some point  
$f_i$ = the fraction of planets with life that actually go on to develop intelligent life (civilizations)  
$f_c$ = the fraction of civilizations that develop a technology that releases detectable signs of their existence into space    
$L$ = the length of time for which such civilizations release detectable signals into space  



L is sui generis among the equation’s factors for two reasons:   
1. It is dependent on sociology, not astronomy or biology (the only other term that is similar in this regard is $f_c$, the fraction of intelligent species that develop a technical civilization).  
2. It is arguably the term that we know, and perhaps can know, least about.  

Motivation:  
- Having some inkling of what L might be—even if that estimate has an uncertainty of a magnitude or two—is significant in motivating (or perhaps demoralizing) those seeking evidence of intelligence elsewhere.  
- Considering the value of $L$ helps us know whether our species (or at least our culture) can reasonably hope for a long future.    




 interstellar colonization cannot hope to solve the problems created by a rapidly swelling population.

 population growth at this time was actually hyperexponential, 

 “medicine will always come before nuclear engineering,” population pressures will always precede any ability for interstellar travel, 

the world population began a sharp rise in growth rate in about 1960 that is predicted to abate by 2050, a century later. ...  the time required for the worldwide shift to democratic governments is similarly a century or so. ... our situation is precarious for the next 150–200 years, and therefore unless we change our social behavior, we “have a high probability of becoming extinct” within that interval.  



### Lamarckism / Lamarckian process  
Lamarckism, a theory of evolution based on the principle that physical changes in organisms during their lifetime—such as greater development of an organ or a part through increased use—could be transmitted to their offspring.[1]  

References:  
[1]: https://www.britannica.com/science/Lamarckism