History and Science after the Chronometric Revolution  
David Christian  


## Part1:  
17th century, Isaac Newton managed to explain the movements of objects **both in the heavens and on Earth**.    

During the Enlightenment era, and for much of the 19th century, European scholars in many different fields, from history to sociology to economics, tried to imitate the success of science by finding “scientific” laws that would explain human history as successfully as Newton’s laws had explained the workings of gravity.  

the end of the 19th century, ... more and more historians gave up hope of finding fundamental laws of historical development.  

历史学与数学物理的研究方法不一样：  
History didn’t allow the repeatability or predictability of laboratory experiments. Instead, it tried to reconstruct a vanished past consisting of millions of apparently unique events.   ... “the human sciences, such as history, have a distinct set of problems. Any analogy to natural science falters because the historian or sociologist, even the economist, cannot effectively isolate the objects of inquiry ... Humanists study action which is responsive to intentions, whereas naturalists investigate the bounded world of behavior.”    
(所以，建立历史的数学模型，并用计算机模拟之，会成为历史研究的新方法？)


## Two chronometric revolutions
2 chronometric revolutions that transformed the discipline of history:  
- One occurred several millennia ago, after the appearance of writing. Written records made it possible to assign absolute dates to events many generations in the past. 发明文字。  
- The second revolution occurred soon after World War II.  It allowed us to assign reliable absolute dates to events  extending back to the very origins of the universe. 同位素测定。  

## Consequences of the chronometric revolution

Are there common themes shared by all historically oriented disciplines, from cosmology to history? One promising (and positive) answer to this question is the idea of “complexity.” 

What is complexity? complex things have three crucial properties: 
1) diverse components organized within a very precise structure; 
2) significant, and increasingly dense energy flows; and 
3) emergent properties that arise from the specific way in which components are arranged.

From above complexity's property 3), then the question is:  
What are the emergent properties that distinguish human history from these other forms of history?  
Our species is distinguished by 2 “emergent” properties:   
- ability (1): to adapt to different environments.   
- ability (2): to seek and find “meaning” in our surroundings.     

collective learning (or culture): the ability to share learned information with precision and in great amounts.    
collective learning (or culture) --> ability (1)       
(1) ----(is linked to, by sharing of symbols)--> (2)      

collective learning can help explain ability (2): symbolic thought  -->  ability (2)

ability (1) --> control over more energy and resources --> more complex.  

the expanded past revealed by the chronometric revolutions allows us to redefine our sense of the past in general and of human history in particular.    


What is the source of our astonishing ecological precocity?   
Why is our species so good at finding new ways of adapting to our environments?   
一个可能的答案：  
- (基础)(in the species scale, for all life)All living species “adapt.” Indeed, “adaptation” is a distinctive emergent property of life in general. Adaptation means that, over time, the average features of a species can slowly change so as to ensure that each individual member can extract from its surroundings the energy needed to maintain its complex structures. This is what natural selection is all about.But it’s a slow process, taking many generations. Individuals don’t adapt; what adapt or change are the average qualities of entire species, as the genes of individuals undergo tiny changes from generation to generation.  
- (人类特有的)(in the individual scale) Learning (a second type of adaptation).  They learn, and that means they get better at dealing with their environment and extracting the energy and resources they need to survive. This form of adaptation is much faster, but it is also Sisyphean. Learning is limited.  
- Cultures (or collective learning). our species is characterized by this new emergent properties that from the way we are linked through the webs of information that we describe as “cultures.”  

This slow accumulation of information—ecological, social, and artistic—generation by generation explains the directional nature of human history. It explains why human history, unlike that of
chimps, is cumulative. It is this process of accumulating social knowledge that has taken us, in 200,000 years, from the low energy consumption of the early Stone Age, to the dangerously high consumption levels of today. Collective learning is a new and extraordinarily powerful adaptive mechanism that is unique to our species. 
(从以上可以推断：知识共享是发展趋势。多文化之间的碰撞及融合是发展趋势。)  

Why are we able to communicate so efficiently?
Because we use symbols as languages. Given our gift for symbolic language, we can:    
- think in symbols which contains information in high-compressed (or complex) forms;   
- communicate information about things that are not present (e.g. imagination), or things that never have existed (e.g. religion).   
 - let symbols work together as larger intellectual structures - models of reality. Sharing these models integrates the learned knowledge over many generations.   
([问题]对比此，很多动物借助气味交流。它们会发展出怎样的文明？)    

There is a linking between:    
- our species’ capacity for “meaning” (our sense of meaning derives precisely from the sense that different parts of reality are linked within our shared conceptual maps) 
- our extraordinary ecological virtuosity (which derives from the sophistication and richness of the models of reality that we construct collectively). 

"Meaning" is a social property, because it depends on the sharing of ideas and language.  

our capacity to share complex models of reality (collective learning (or culture)) --> Our sense of meaning --> our extraordinary ability to keep adapting to the physical world in new ways.  


## 原始人的energy consumption level:  
When humans first appeared about 200,000 years ago, there may have been just a few thousand of them, each using at least 3 Kcals/day. ... Today, there are more than 6 billion of us, and on average each of us is controlling approximately 230 Kcals a day, or more than 70 times as much as our Stone Age ancestors  