# The Intelligent Universe
James Gardner

Two factors, above all others, shaped human history in the 20th century:  
 -  flourishing of the natural sciences and technology, 
 - “the great ideological storms that have altered the lives of virtually all mankind: the Russian Revolution and its aftermath—totalitarian tyrannies of both right and left and the explosions of nationalism, racism, and, in places, of religious bigotry.”

memetic engineering (analogous to genetic engineering):  
to manipulate complex patterns of replicating memes and achieve consistent and predictable manifestations in the form of a precisely altered cultural phenotype.    
memetic engineering's goal: 
to ensure that the better angels of our nature prevail in the strange new transhuman cultural environment after singularity.  


to place life and intelligence at the center of the vast, seemingly impersonal physical processes of the cosmos.  

memetic engineers (with the concept of an intelligent universe) will build the cultural foundation for a benign cosmic future.  

Butler challenge:  
- to understand/model the future course of a cultural evolutionary process that will supersede the human stage of evolution and  transcend our human capacity to understand its dynamics in microscopic detail.  
- to perturb/shape the future process of cultural evolution by means of which Mount Invisible will arise in a manner that will render the entire edifice human-friendly over the long term.  

Whether the human mind is capable of meeting the Butler challenge is an open question. ... humanity has perhaps three decades to prepare to meet the Butler challenge.  

Approach(s) dealing with Butler challenge:  
- Singularity Institute (Bottom-up). That is, when we have not yet succeeded in creating strong AI, let more scientists focus on this topic, build a set of algorithms into the strong AI’s source code to prevent it from against human beings. 
- Memetic Engineering With Cultural Attractors (Top-down). That is, to design a set of cultural attractors that could conceivably perturb the developmental direction of the future cultural environment in which strong AI will emerge in such a way as to encourage the prolongation of human-friendly sensibilities and outcomes.


### Alfred North Whitehead’s (cultural attractor?) theory 
“the faith in the possibility of science, generated antecedently to the development of modern scientific theory, is an unconscious derivative from medieval theology.” More specifically,  
"the greatest contribution of medievalism to the formation of
the scientific movement [was] the inexpugnable belief that
every detailed occurrence can be correlated with its antecedents
in a perfectly definite manner, exemplifying general principles.
Without this belief the incredible labours of scientists would
be without hope. It is this instinctive conviction, vividly poised
before the imagination, which is the motive power of research—
that there is a secret, a secret which can be unveiled."  

注：Middle Ages: 500 CE - 1400 or 1500 CE.  哥白尼（1473-1543）。  
伽利略（1564-1642）、开普勒（1571-1630）、Newton(1643-1727) 在此之后。

The source of the conviction was ... a peculiarly European habit of thought—a deeply ingrained, religiously derived, and essentially irrational faith in the existence of a rational natural order.  

The scientific sensibility was a belief in the existence of a well-ordered universe that abides by invariant natural laws which can be discovered by dint of human investigation.   

cultural attractor:  
- the rational nature of the universe
- the concomitant possibility of human scientific discovery
作者的思路是以上面的cultural attractor，类比推出：
a complementary cultural attractor: 
- human 
- the emergence of human-friendly AI

Gaia principle/theory/paradigm/hypothesis:   
It proposes that living organisms interact with their inorganic surroundings on Earth to form a synergistic and self-regulating, complex system that helps to maintain and perpetuate the conditions for life on Earth.  

作者的观点被称为"Selfish Biocosm hypothesis":
the ongoing process of biological and technological emergence, governed by still largely unknown laws of complexity, could function as a von Neumann controller and that a cosmologically extended biosphere could serve as a von Neumann duplicating machine in a conjectured process of cosmological replication.

The author speculate that:  
the hypothesized cosmological replication process could occur through the fabrication of baby universes by highly evolved intelligent life-forms. These hypothesized baby universes would themselves be endowed with a cosmic code—an ensemble of physical laws and constants—that would be life-friendly so as to enable life and intelligence to emerge and evolve.  



### closed timelike curve (CTC)

###  super-Copernican principle
the future can have at least as important a role in shaping the present moment as the past.  


if our cosmos is indeed a CTC—or if our multiverse is a series
of branching CTCs—then the human-dominated past will  ontinue to exert a causal effect on a transhuman future, long after humanity ceases to be the dominant form of intelligence on planet Earth.