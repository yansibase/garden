cultures evolve over time.    
how are we to explain the patterns to be found in that database (cultural changes through history)?   
Are there any good theories or models of cultural evolution?  

it is a mistake to assume that the “cultural selection” of a cultural trait is always “for cause”—always because of some perceived (or even misperceived) benefit it provides to the host.

We are genetically endowed with a biased quality space: some things feel good and some things don’t. 


#### compare this development to the revolution that happened among the bacteria
Relatively simple prokaryotes
got invaded by some of their neighbors, and the resulting endosymbiotic teams
were more fit than their uninfected cousins, and prospered. These eukaryotes,
living alongside their prokaryotic cousins, but enormously more complex,
versatile and competent thanks to their hitchhikers, opened up the design
space of multicellular organisms. Similarly, the emergence of culture-infected
hominids has opened up yet another region of hitherto unoccupied and
untraversable design space. We live alongside our animal cousins, but we are
enormously more complex, versatile, and competent. Our brains are bigger, to
be sure, but it is mainly due to their infestation by memes that they gain their
powers. 


##### Natural Selection of Musical Memes
某个原始人A无故敲击发出声响。周围其他的原始人B和C可能模仿A敲击出类似的声响。这种敲击逐渐变成一种习惯H。H衍生出变种习惯H1、H2。
如果H是有益的（声响好听），则H会持续传播。  
如果H是有害的（声响难听），则“如果H1的有害程度小于H，那么H1会取代H“。  
最终，若传播条件允许，则H或其变种会传播。The drumming virus is born。  


##### Unconscious Selection of Memes
Some of the drummers begin to hum, and of all the different hums, a few are more infectious than the rest. 例如原始人A发现了若干种敲击方式M1、M2，M1比M2更好听。A因敲击方式M1而更受同伴欢迎。  A competition between different humming patterns emerges. 
if varying tempo and pitch of one’s hums **feels good**, and also happens to create a ready **supply of more attention-holding noises for spreading to conspecifics**, **one’s primitive aesthetic preference can begin to shape, unconsciously**, the lineages of humming habit that spread through one’s community.
Brains in the community begin to be infected by a variety of these memes. Competition for time and space in these brains becomes more severe. The infected brains begin to take on a **structure** as the memes that enter “learn” to cooperate on the task of turning a brain into a proper meme-nest with lots of opportunities for entrance and exit (and hence replication).18 Meanwhile, any memes out there “looking for” hosts will have to compete for available space therein. Just like germs.


##### Methodical Selection of Memes
As the structure grows, ... the brains of the hosts ... become evermore potent and discerning selective agencies ...... Some people ... are better at this than others.    
A music talent knew how to breed new strains of music from old. ...... memes that had already sunk their hooks deeply into the emotional habits and triggers of the brains where they had been replicating for years. Then he used his technology to create variations on these memes, seeking to strengthen their strengths and damp their weaknesses, putting them in new environments.  

##### Memetic Engineering


