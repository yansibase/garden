# Bringing Cosmos to Culture - Harlow Shapley and the Uses of Cosmic Evolution
JoAnn Palmeri

Institute for Religion in an Age of Science (IRAS). IRAS emerged from the combined efforts of individuals associated with the “Coming Great Church” conference and members of the American Academy of Arts and Sciences.  

Cosmography: a discipline based in a cosmic way on chemistry, physics, social biology, geology, astronomy, all referred to the fundamental physical entities of space, time, matter, and energy. ... it treated all the components of the cosmos and all the sciences with emphasis on connections, classification, and a common evolutionary framework.  

*Of Stars and Men: The Human Response to an Expanding Universe*  

 Historians have documented the increasing importance of religion in American life in the period following World War II. As Stephen Whitfeld has argued in discussion of the revival of religion in American culture during the Cold War, what was revived in this period “was not so much religious belief as belief in the value of religion.”  




