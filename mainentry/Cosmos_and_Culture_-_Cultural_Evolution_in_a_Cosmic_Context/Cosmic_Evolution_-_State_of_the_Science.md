
#### 1. Candidates for characterizing complexity:  
- information content  
- negative entropy  
- energy:  energy seemingly has a central role to play in any attempt to unify physical, biological, and cultural evolution.  
- "energy density (energy per mass)" or "energy rate density" or "energy flow rate": higher energy density may reflect higher complexity.    


#### 2. "nonrandom elimination" (or "selection")  
aggregate of adverse circumstances responsible for the deletion of some members of a group.   


#### 3. How has complexity actually increased in evolution?
- "energy density": driving an initial system beyond equilibrium,   
- "nonrandom elimination": aiding the emergence of higher order in that system.    

Energy density is the trait most often selected by successful systems of the same kind. For example, life-forms require the acquisition of more energy per unit mass for their well-being. (My example, people prefer sweeter fruit, because sweeter fruit has more sugar which in turn has more energy. )   ^03b414

[[Evo_Devo_Universe_-_A_Framework_for_Speculations_on_Cosmic_Culture#About Energy Flow Density]]