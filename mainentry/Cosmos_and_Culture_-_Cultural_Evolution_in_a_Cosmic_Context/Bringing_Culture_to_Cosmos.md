Bringing Culture to Cosmos - The Postbiological Universe  
Steven J. Dick

British philosopher Olaf Stapledon  

Table 1:    

| Topic in Human Thought | Time Scales                               | Example         |
|------------------------|-------------------------------------------|-----------------|
| Human                  | 100                                       |                 |
| Historical             | 10,000 years                              |                 |
| Anthropological        | 10 million years                          |                 |
| Geological             | 5 billion years                           |                 |
| Astronomical           | 14 billion years                          | SETI proponents |
| Stapledonian           | Biology and Culture on Astronomical Scale |                 |

methodological premises of this paper:  
- (1) long-term Stapledonian thinking is a necessity if we are to understand the nature of intelligence in the universe today.  
- (2) cultural evolution must be seen as an integral part of cosmic evolution and the Drake Equation.  

three scientific premises in the arguments for a postbiological universe:  
1) the maximum age (A) of ETI is several billion years;   
2) the lifetime (L) of a technological civilization is >100 years and probably much larger;   
3) in the long-term, cultural evolution supersedes biological evolution, and would have produced something far beyond biological intelligence.   

 SETI proponents wish to search for intelligence using current technology, so they prefer the option that extraterrestrials will have technology similar to ours. That is an option, but only one of many, and, possibly, not the most likely scenario. 

![](img/Drake_equation.jpg)  

postbiological universe: an universe in which the majority of intelligent life has evolved beyond flesh and blood intelligence, in proportion to its longevity, L.  

WMAP: Wilkinson Microwave Anisotropy Probe.    

L (in Drake equation) is a problem of cultural evolution, and cultural evolution must be taken into account no less than astronomical and biological evolution. ...  L need only be thousands of years for cultural evolution to have drastic effects on civilization.  

modern Darwinian theories (all have considerable problems... **for now a widely accepted theory or mechanism of cultural evolution is lacking**):    
- sociobiology (Wilson 1975), “the systematic study of the biological basis of all social behavior.”  
-  gene-culture coevolution  
- “dual inheritance” theory (Boyd and Richerson 1985), uses population genetics to construct simple mathematical models of how cultural evolution works. The authors recognize, however, that their system cannot yet make quantitative predictions, but can only clarify the relationships between cultural transmission and other Darwinian processes.  
- “Universal Darwinism”(Dennett 1996): Darwinism applies to humans at many levels—mind, language, knowledge, and ethics . When applied to knowledge and its transmission, it leads to the field of “memetics,” ... Despite a number of books and a Journal of Memetics, **even memetic enthusiasts realize the field is far from a real science** (Aunger 2000).   
- “emergence”. culture or its components (toolmaking, language, agriculture, technology, and so on) are emergent phenomena that will be explained in terms of agents, rules and “pruning relations” in the way that the origin of life and the origin of consciousness may someday be explained as emergent phenomena.  

## Intelligence Principle
the central principle of cultural evolution - Intelligence Principle:   
the maintenance, improvement and perpetuation of knowledge and intelligence is the central driving force of cultural evolution, and that to the extent intelligence can be improved, it will be improved.  

## 关于道德  
It may well be that Moravec, Kurzweil, and their proponents **underestimate the moral and ethical brakes on technological inertia**. (道德牵制科技发展)    

Based on what experts see happening on Earth, L need not be five billion, one billion, or a few million years. It is possible that a postbiological universe would occur if L exceeds a few hundred or a few thousand years, ...   

## Lifetime of a Technological Civilization and Effects on SETI
- If L < a few hundred years, less than the time it takes for a technological civilization to conceive, design, construct, and launch their intelligent machines, we do not live in a postbiological universe. 
- If  100 < L < 1,000 years, a transition zone may result populated by human/machine symbiosis, sometimes referred to as “cyborgs”, and genetically engineered humans. 
- if 1,000 years < L, we almost certainly will have made that transition to a postbiological universe (Table 2).   
  
| L (Years)  |  Stage of Cultural Evolution    |  Effect on SETI                                             |
|------------|---------------------------------|-------------------------------------------------------------|
| < 100      | Biological                      | Civilizations scarce but comparable level—EM SETI possible  |
| 100 ~ 1000 | Machine/Biology Hybrid (Cyborg) | Hybrid techniques                                           |
| 1000 <     | Postbiological                  | Advanced artificial intelligence -- Direct EM SETI unlikely |

the universe over the billions of years that intelligence has had to develop will not be a biological universe, but a postbiological universe. Biologically based technological civilization as defined above is a fleeting phenomenon limited to a few thousand years ... only 1 in 10^6 civilizations are biological.  


In the tradition of Stapledon, and guided by the Intelligence Principle, a postbiological universe would be like:  
- Immortality.   
	the capability of repair and update, capabilities facilitated by their modularity.  
- High intelligence.   
	this intelligence is cumulative, that is, the sum total of knowledge in the parent machine is passed on to the next generation.   
- increased tolerance to the environment (e.g. vacuum, temperature, radiation, or acceleration ...).  

with cultural evolution, AI --> “spirit”(cosmotheology)  

For SETI, the postbiological universe imples that:   
- the problem of search space.  
	SETI searches for postbiologicals need not be confined to planets, nor to planets at all. Artificial intelligence, or their robotic surrogates, could roam the galaxy as reproducing von Neumann machines. ... they might be more likely to roam the universe than to send signals.      
- the nature of the signal.  
	the long lifetimes of artificial intelligence “could be very advantageous for interstellar contact among advanced communities.  
- emphasis on message construction.  
	postbiologicals might be more interested in receiving signals from biologicals than in sending them.  
-  Incommensurability Problem.  
	 the differences between biologicals minds and postbiologicals are so great that communication is impossible.

  - 
L (in Drake equation) parameter is a double-edged sword for SETI.  

All of these conclusions, and the possibility of a postbiological universe in general, point to the need to place AI research in a cosmic context. ... With the symbiosis of SETI and AI, SETI expands its possibilities into new phase space.  

## Summary  
- if the lifetime of technological civilizations typically exceed 1,000 years, it is likely that we live in a postbiological universe.  
-  if such intelligence does arise, cultural evolution must be taken into account, and that this may result in a postbiological universe.  
- if we live in a biological universe, the extraterrestrials that compose the biological universe would be millions, if not billions, of years older than us.  
