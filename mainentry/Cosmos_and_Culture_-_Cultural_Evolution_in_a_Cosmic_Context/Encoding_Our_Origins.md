Encoding Our Origins - Communicating the Evolutionary Epic in Interstellar Messages

Douglas A. Vakoch

International Academy of Astronautics (IAA)  

Interstellar Message Construction Study Group

two distinctive features of evolutionary explanations of nature:
- change;  
	Though there may be constant laws of nature, these laws are manifested through transformations of the stuff of the universe:
- historical embeddedness;  
	The natural order we see today depends, at least in part, on historical circumstances


interstellar recording attached to two Voyager spacecraft,

In 1961, Frank Drake, sent a message consisting of 551 ones and zeros to each of the participants

 In 1974, when Drake transmitted an actual message from the world’s largest radio telescope and radar facility, located near Arecibo, Puerto Rico, 

Freudenthal’s (1960) interstellar language [LINCOS](https://en.wikipedia.org/wiki/Lincos_language).  