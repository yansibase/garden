# Cosmocultural Evolution - The Coevolution of Culture and Cosmos and the Creation of Cosmic Value
Mark L. Lupisella

- Is culture merely an increasingly complex result of biologically driven self-interest, arising from the happen-stance of life?
- Is culture merely a blind walk (or run?) of replicating memes—the cultural equivalent of natural selection?

Goals:  
 1) to provide a basic framework for thinking about how culture and cosmos might relate—the primary notion being “**cosmocultural evolution**” and/or the **Cosmocultural Principle**; 
 2) to develop the notion of “**bootstrapped cosmocultural evolution**,” including practical near- and longer-term implications; 
 3) to suggest a long-term worldview, consistent with 1) and 2), that can be characterized as a **morally creative cultural cosmos**—a post-intelligent, post-technological universe that enters the realm of conscious evolution driven largely by moral and creative pursuits.  


culture: **collective manifestation of value**.   
- "Value" means something valuable to “sufficiently complex” agents. Meaning, purpose, ethics, and aesthetics can be derived from "value".    
- Culture manifests value in many varied forms, from thoughts and knowledge to symbolic abstractions to social norms to mass movements to large-scale physical creations.  
- “Collective” is that which is shared, which suggests:  
	- a) at least some degree of common interests, pursuits, or purposes among multiple agents, including future generations; and   
	- b) the transmission of information in space and time, including across generations— what might be thought of as a kind of collective memory.  
- “Manifestation” suggests instantiation in the world—e.g., thoughts, behavior, and objects (including purely aesthetic objects) that are predominantly (but not exclusively) driven by some usefulness to agents—e.g., to perform a function, adapt, anticipate, and modify memory, information, and knowledge in order to more effectively pursue interests.   
- “Sufficiently complex agents” implies beings with interests that are capable of complex autonomous behavior to pursue those interests.  

“cultural evolution”: the variance of culture over time.   

![](img/relationships_between_cosmos_and_culture.jpg)

a bootstrapped cosmocultural perspective suggests:  
- the universe has bootstrapped itself into the realm of value and culture via valuing cultural agents such as ourselves, 
- the significance and potential for cultural evolution is unlimited.

benefits to migrating off Earth:  
- Humanity needs to do the difficult experiment of migrating off Earth to assess if and how we can effectively and sustainably survive and thrive outside the comforts of our natural biosphere.  
- Human beings have slowly, and perhaps sometimes too painfully, benefited from social experimentation that has often been driven and accelerated by migrating to new environments, with new challenges, and new freedoms.   
- Experimenting with new forms of social organization and new means of governance can benefit from the challenges of migration—especially to challenging environments.   
- Migrating into the wider universe can serve that purpose and help unite all countries of the world in a common, perhaps critical, long-term endeavor of human expansion and social experimentation.
- cultural diversity.  

### Different versions of Kardashev Type IV
- Milan Ćirković (2004) suggests that Type IV should be used to designate a civilization that can harness the power of its supercluster;  
- Michio Kaku (2005) suggests a Type IV civilization could harness extragalactic energy sources such as dark energy; 
- Zoltan Galantai (2004) has suggested a Type IV level which harnesses the energy of the visible universe.

### A criterion for measuring the civilization  
the **kind** of impact and influence a culture exerts on its environment and the universe.  
- Type I Influence: Planetary.    
 Be able to influence a planet and solar system bodies. Examples:  
	- biospheric control, 
	- defense from astronomical impacts such as asteroids, etc.
- Type II Influence: Astrophysical.   
	Be able to use, control and modify astrophysical objects on small and large scales—e.g., stars and galaxies, superclusters, possibly black holes, etc. Examples:
	- the ability to harness most if not all of a star’s energy, 
	- control the energy output of a star, 
	- extend the lifetime of stars, 
	- modify the composition of stars, 
	- control the energy of galaxies and superclusters, 
	- possibly create black holes, 
	- harness unusual forms of energy such as “dark energy.”   
- Type III Influence: Cosmological.  
	Be able to influence and control phenomenon on cosmological scales, i.e., the large-scale behavior of the universe, but within the constraints of physical laws and constants. Examples:
	- extending the lifetime of the universe (perhaps by slowing or accelerating expansions or contractions) possibly transmitting something like information through a big crunch, creating baby universes, or creating an information processing universe and/or a kind of cosmic mind.
-  Type IV Influence: Ontological.  
	Be able to control and modify the physical nature of the universe itself—truly “mind over matter.” Examples:  
	- an ability to change physical constants and perhaps laws.  
- Type V Consequence: Metaphysical [^1] .  
	A very broad (perhaps the broadest) category.  It includes:   
	- Ontology. (Things that actually exists (primarily physically)).  
	- Nonphysical things. It includes:     
		- Platonic realm. Examples: mathematical constructs, logic, redness, etc.  
		- Theology. Examples: God, etc.  
		- Things like value, meaning, purpose, divinity, “spirit,” etc.   





### 关于道德
As intelligence and technology carries beings to ever-increasing degrees of well-being and comfort, the cost of caring for others can decrease, helping to make it easier to care for others, resulting in more caring acts and an increase our overall “caring capacity.”.... As the cost of caring for others is reduced, we may be able to better pursue the well-being of all as a critical organizing principle for cultural evolution,
(我不同意作者对moral的看法，而倾向于[Steven Dick关于道德](./Bringing_Culture_to_Cosmos.md#关于道德)的观点。我认为：在社会层面，道德是科技发展的负反馈。没有道德约束时，科技发展速度可能会像脱缰的野马一样快、可能无法悬崖勒马，所以需要道德作为缰绳来勒马。)

### 关于创造力  
if, or when, our “caring capacity” has been reached, if the well-being
of all has been sufficiently achieved, what then? ...we see signs
of cultural evolution that may point us in directions. ... Increasing creativity may be one of those directions.


Notes:  
[^1]  “metaphysical” may be used in a somewhat nontraditional philosophical sense. [1]  

References:  
[1]. Mark L. Lupisella, *Cosmocultural Evolution - The Coevolution of Culture and Cosmos and the Creation of Cosmic Value* in the book *Cosmos and Culture - Cultural Evolution in a Cosmic Context*. 2009.  
