#### 术语
evolutionary developmental (evo-devo)
evolutionary, information-processing, and developmental: “evo compu devo” (abbreviated “evo devo” hereafter)  
CAS: complex adaptive systems   
ECD: Evolution Compu Development.  
STEM: space, time, energy, matter.  
STEM dense: space, time, energy, matter dense.  increasingly localized in space, accelerated in time, and dense in energy and matter flows.  
STEM efficient: in space, time, energy, and matter resources used per standardized computation or physical transformation.  
DS: developmental singularity.  Earth’s local intelligence will apparently very soon in astronomical time develop black-hole-analogous features, a highly local, dense, and maximally computationally efficient form.  
dissipative structures: complex adaptive systems that use energy flows.  



**evolutionary processes** that are stochastic, creative, and “divergent,”   
**developmental processes** that produce statistically predictable, robust, conservative, and “convergent” structures and trajectories,  

#### three simple models of universal change  
![](img/ICU_EDU_DS.jpg)
##### ICU hypothesis
the informational computational universe (ICU) hypothesis, considers the universe as a purposeful information processing system in which biological culture, as it arises throughout the universe, has the potential to play some integral (e.g., anthropic) yet transient universe-guiding role.

#####  EDU hypothesis
the evo devo universe three models of universal (EDU) hypothesis, considers the universe change. as engaged in both processes of evolutionary creativity and processes of hierarchical development, including a specific form of accelerating hierarchical development we call “STEM compression” of computation.  

#####  DS hypothesis
the developmental singularity (DS) hypothesis, proposes our universe’s hierarchical and energetically dissipative intelligence systems are developmentally constrained to produce, very soon in cosmologic time, a very specific outcome, a black hole analogous computing system. Per other theorists (see Smolin 1997) such a structure is likely to be a core component in the replicative life cycle of our evo devo universe within the multiverse, a postulated environment of universes.


#### evo devo universe hypothesis
- The ICU hypothesis (in some variant) as outlined earlier.  
- The Evo Devo Analogy.   
- Definition of Evolutionary Processes.    
  intrinsic systemic unpredictability. The dynamics of evolutionary change are random within constraints, ...... In the universe at large, any process with unpredictability, contingency, generative creativity, and divergence may be a candidate for being  evolutionary.   
- Definition of Developmental Processes.    
   systemic predictability.  ...... In the universe at large, any process with predictability, macrodirectionality, and convergence, or any process with a predictable beginning, ending, and rebeginning (either demonstrated or expected) may be a candidate for being developmental.  
-  Evolutionary and Developmental Interactions and Functions.   
	- Evolution
		- Evolution creates novel developmental architecture, but does so very slowly, over many successive developmental cycles. ...... Evolution is also constrained to act in ways that do not disrupt critical developmental processes or terminate the life cycle in each generation. ......evolution: (variation of form) 
		- evolution is basic or neutral information/ intelligence creation and variation, what may be called preadaptive radiation, parameterization, and experimentation, not selection.  
	- Development
		- development: (continuity of form)
		- development is information/intelligence preservation (system sustainability), which it does via hierarchical emergence and intelligence transmission to the progeny.  
	- Evo Devo
	    -  creates information, natural selection, adaptation, plasticity, and universal intelligence.
	    - as complementary modes of information processing in all complex adaptive systems, including the universe as a system.  
	    -  is a complex system’s way of learning and engaging in natural selection, or “meaningful” information/intelligence accumulation, thereby adapting to and shaping its environment to the greatest extent allowable by that system’s internal structure and external environment. 
	    - connections emerging between natural selection and information theory. 
			- The evo devo process of natural selection, as it “learns” which of many varieties are most fit for a niche, can be said to create information (reduction of uncertainty) 
			- biological natural selection leads reliably to increased variety or diversity of extant forms over time.  
    - ECD: Evolution Compu Development
	    -  traditional neo-Darwinian view V.S. ECD: 
		    - traditional neo-Darwinian view: evolution is described as a quintessentially adaptive process, and is equated with natural selection on phenotypes in a competitive environment
		    - ECD proposes that divergent variation (change-creating experimentation) is the essential evolutionary process. We reclassify natural selection (adaptation) as an evo devo process. ......  
		- ECD triad model proposes that what biologists typically call “evolution” can be usefully analyzed as three distinct simultaneous processes: evolution, natural selection/adaptation/evo devo, and development.
- The Evolutionary Development of Self-Similar Hierarchical Substrates: A Generic Quintet Hierarchy.  
- Evo Devo in Creation(95%) and Control(5%).  e.g. evolutionary genes(majority) v.s. developmental genes(minority).  
- Evo Devo, Life Cycle, and Intelligence: Seed, Organism/soma, and Environment (SOE) Intelligence Partitioning.  
	- if our universe is an evo compu devo system, it must also be energetically and informationally partitioned between:
		- a germline (seed) of parameters and special initial conditions which replicate it,   
		- a finite universal body (soma) that is initially adaptive then grows increasingly senescent with time,   
		- a surrounding environment (the multiverse).  
	- If our universe is developmental it must also have some mechanism of replication. 
-  Cosmological Natural Selection (CNS): A Promising Yet Partial Evo Devo Universe Hypothesis.  
	- any change in developmental genes is almost always either deleterious or catastrophic, particularly in more complex organisms.  ...... some fundamental parameters of physics appear very sensitively tuned to sustain our universe’s internal complexity, with small changes being catastrophic to complexity emergence (a.k.a.  “Fine Tuning Problem”). In the EDU hypothesis, such parameters seem clearly developmental.  
- Evo Devo Cosmological Natural Selection with Intelligence (Evo Devo CNS-I).  


#### Three things to understanding the DS hypothesis
##### 1. Universal Development as Differentiation and Terminal Differentiation (aka, Cosmogonic Philosophy).   
rate of diversity innovation is drastically reduced at all levels, and has stopped entirely at all the older, lower levels.    

the tree of biological developmental differentiation on our planet has nearly reached its maximum height.    

##### 2. Universal Development as STEM Compression of Computation in Dissipative Structures. 
STEM compression: trends of density and efficiency in space, time, energy, and matter.     ^a28349

###### 2.1 Space Compression (or  locality, or smaller&restricted spatial zones). 
e.g.  universally distributed early matter --> galaxies --> replicating stars within galaxies --> solar systems in galactic habitable zones --> life on special planets in those zones --> higher life within the surface biomass --> cities --> intelligent technology --> global digital networks, sensors, effectors, memory, and computation.  

gravity - a process of space compression around massive objects.  seems to be a basic driver (an integral aspect) of universal computational development.  

###### 2.2 Time Compression.   
e.g. the speed of biological thought <  the speed of electronic “thought”.  

###### 2.3 Energy Compression.    
all dissipative structures (complex adaptive systems that use energy flows), can be placed on an apparently developmental universal emergence hierarchy (galaxies --> human societies --> ...), with earlier-emerging systems having far less free energy flow (Phi) than recently emerging systems. Free energy is energy available to build structural, adaptive complexity. Phi can be considered a measure not of structural complexity but of dynamic complexity, or marginal learning capacity of the dissipative structure. It also seems closely related to marginal entropy production.    

energy density and efficiency may be considered through Constructal theory, which proposes that for any finite-size system to persist in time (to live), “it must evolve [and develop] in such a way that it provides ever easier access to the imposed currents that flow through it.”   

###### 2.4 Matter Compression.    
e.g. Early life and pre-life-forms must have been far less genomically and cellularly efficient and dense. DNA folding and unfolding regimes in every eukaryotic (vs. prokaryotic) cell are a marvel of material compression (efficiency and density of genetic computation) .  

##### 3. Universal Development as Ergodicity (aka Computational Closure)


#### Developmental Singularity (DS) Hypothesis
It includes the following claims and subhypotheses,
- The ICU and EDU hypotheses, in some variation,  
- The Developmental Singularity (aka Inner Space or Transcension) Hypothesis: An Asymptotic Mechanism for Universe Simulation and Reproduction.  
- Black Holes as Ideal Structures for Information Gathering, Storage, and Computing in a Universe that is Increasingly Ergodic to Local Observers.  
-  Black Hole or Nonrelativistic Computronium Mergers as Mechanisms for Intrauniversal Natural Selection in Evo Devo CNS-I.
-  The Coming Challenge of Postbiological Intelligence: The Evolutionary Development of Friendly AI.




#### 其他
Current research (Aaronson 2006, 2008) now suggests that building future computers based on quantum theory will not yield exponentially, but only quadratically growing computational capacity over today’s classical computing. 

It seems to me that the most productive human beings in mid-21st century society, as well as most of our youth, will increasingly depend on their cybertwins (personal digital  assistants)  as their primary interface to the world. It also seems likely that many of us will allow our cybertwins to continue to increase in complexity and usefulness to society even after our biological bodies have died, which will profoundly change the nature of grieving and the social impact of death. At some point, with the advanced nanotechnology that postbiological life seems likely to command, our cybertwins can permeate our biological brains and bodies with their nano-sensor grids, develop deep connectivity between our digital and biological identities, and deliver a kind of immortality, even a subjective immortality, by successive digital approximation. (1.这可能和元宇宙有联系。2.已经有这类题材的影视所品，比如《Upload》)

 electronic computers, have roughly seven orders of magnitude (10 millionfold) greater free energy rate density than human culture. ...... It seems unbelievable that our Sun has two orders of magnitude less Phi than a houseplant.


##### A system’s level of complexity
Chaisson’s view:  
A system’s level of complexity is ... its ability to channel matter and energy, per mass or volume, per time, for info/learning/adaptation (alternatively, evo and devo) activities.
![](img/Phi_of_systems.jpg)  

##### Evolutionary Developmental Processes of X  
repeat the cycle (replicating/copying, varying, selecting) on X.  

#### About Energy Flow Density
[[#2 3 Energy Compression]]

[[#A system’s level of complexity]]