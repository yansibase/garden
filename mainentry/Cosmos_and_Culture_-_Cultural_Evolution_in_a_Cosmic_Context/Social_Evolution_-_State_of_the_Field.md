
two astronomical principles:  
(AP1) Copernican Principle of Mediocrity (which says that Earth is not special),  
(AP2) the uniformity of the laws of nature throughout the universe.    

premises:
(P1) that evolution on Earth tends to produce complexity; 
(P2) that biological, social, and technological evolution will work the same way elsewhere in the galaxy as they have here; (consistent with AP1 and AP2);  
(P3) that these processes will therefore eventually produce approximately equivalent (not identical) results wherever suitable conditions exist. (consistent with AP1 and AP2);  

P1 + P2 + P3 ==> extraterrestrial intelligence (ETI) with detectable technology may well exist. 


#### 社会科学里，各种不同观点出奇地多。如何看待此现象？
contemporary science is as cultural as shamanism, opera, Catholicism, or the Boy Scouts. It does not stand outside of culture in a space of perfect objectivity. Scientists are people, and they are subject to the influences of their times. Theories explaining the world change diachronically or vary synchronically not only because of variation in the available data, but also because of change and variation in the people producing the theories. Objectivity itself is culturally constituted.  

 “We need to make a distinction between the claim that the world is out there and the claim that truth is out there . . . (because) where there are no sentences there is no truth . . . The world is out there, but descriptions of the world are not. Only descriptions of the world can be true or false.”  



#### "人种(race)"优劣理论的荒谬之处：  
a wealth of research indicates that biological differences between human populations are superficial, and that ‘race’ is biologically meaningless because there is more variation within populations than between them




Diamond contends that instead, European societies were lucky—they just happened to develop in a biogeographical context that led easily to the colonization and conquest of other lands.



#### 人们为何关注Collapse ?   
... not only do human beings like narratives, but we prefer simple, unsophisticated narratives, particularly binary stories of good and evil. ... At any rate, no story and no scientific account can connect all the dots, and so scientists and authors emphasize some points and ignore others, decide when to start and when to stop, and deliver messages in keeping with their worldviews. As we obtain more dots, the data set may enlarge or constrain the number of plausible interpretations. It rarely permits just one.   
In any case of collapse, where we begin and end the story is important. As discussed above, this has implications for our positioning of people alive today, but it also determines where and when we focus our historical research. ... in many ancient societies, it was assumed that human societies had life spans and would someday die only to be replaced by others. Empires were assumed to be unsustainable, and history was therefore assumed to be cyclical. We, on the other hand, have tended to believe that complex societies should endure. Therefore, historical collapses are compelling violations of our expectations; they attract our attention, demand explanation, provoke anxiety, and invite the telling of cautionary tales. We may even invent them sometimes, because we need their dark ink to draw our own mortal fears.     
##### what happens after ‘collapse’?  
in looking at the frequent reemergence of complex societies and rise of second-generation states, they emphasize processes of transformation and regeneration, not simple disappearance 


#### 关于“玛雅文明“的认知偏差  
whether the general ‘Mayan collapse’ is a meaningful concept at all, or whether it was, essentially, invented by previous generations of archaeologists, and then wildly reinterpreted and misinterpreted by others.   
“scientists always use narrative, and thus benefit from being self-conscious narrators. Science cannot make an indefinite number of observations, and so selects, just like a narrator.” ...... not only do human beings like narratives, but we prefer simple, unsophisticated narratives, particularly binary stories of good and evil. ...... At any rate, no story and no scientific account can connect all the dots, and so scientists and authors emphasize some points and ignore others, decide when to start and when to stop, and deliver messages in keeping with their worldviews. As we obtain more dots, the data set may enlarge or constrain the number of plausible interpretations. It rarely permits just one.
##### 认知偏差  
- In any case of collapse, where we begin and end the story is important. ...... As discussed above, this has implications for our positioning of people alive today, but it also  determines where and when we focus our historical research. ... in many ancient societies, it was assumed that human societies had life spans and would someday die only to be replaced by others. Empires were assumed to be unsustainable, and history was therefore assumed to be cyclical. 
- We have tended to believe that complex societies should endure. Therefore, historical collapses are compelling violations of our expectations; they attract our attention, demand explanation, provoke anxiety, and invite the telling of cautionary tales. We may even invent them sometimes, because we need their dark ink to draw our own mortal fears. ...... what happens after ‘collapse’; in looking at the frequent reemergence of complex societies and rise of second-generation states, they emphasize processes of transformation and regeneration, not simple disappearance.  


#### 历史能否预测未来？预测的准确程度如何？
What exactly can these inquiries teach us about the past, or ourselves?
(History) tells us where we came from, and that is useful to know. But does it really go beyond that? 
“how does the history of human-environment systems generate useful insights
about the future?”
(History) tells us where we came from, and that is useful to know. But does it really go beyond that? 
Does the history of human-environment systems really generate useful insights about the future? and if so, what form do those insights take?
what sort of relationship do the past and present really have—i.e., in what sense are they really each other’s analogs?
can our explanations of disconnected events long-past truly be expected to provide useful predictions about our own global system?

 in the historical sciences, explanation and prediction must be teased apart; these disciplines are not like experimental science,
while we need to look to past collapses for information about our own future, they will not give us simple answers about what we should do next. He suggests, rather, that
past cases can help us to understand the general nature of our present systems. ....... one of our tasks in sustainability
research is to know where we are in terms of our own complex system’s trajectory—to know where we are in history, and understand the current state of our own problem-solving capacities (Tainter 1996). This is something that the experiences of past societies can hint at, but not tell us outright. To know, we must look in the mirror, not the microscope.

####  Challenges  which are relevant to culture in the cosmos  
- keep working on the recovery and primary interpretation of data (in history) ;   
- continue developing sophisticated models to integrate those interpretations, appreciating that the more we know, the harder the integration will get;   
- keep questioning the stories told about the past, because they can legitimately, and will inevitably, be told in different ways and the choices have consequences;   
- keep asking exactly how, and to what extent, models of the past relate or apply to our present and future.  


#### societies generally develop traits in ordered evolutionary sequences: Guttman scaling
In a Guttman scale, at the top are traits that imply the presence of the traits listed below them. ...... if traits form a Guttman scale, there are clear evolutionary implications.    
##### Guttman scale of traits sampled randomly from 20 archaeological cases from the eHRAF:    
While not all cases must evolve in precisely this way, a valid Guttman scale cannot occur unless most cases behave in the manner described in the scale
- Writing
- Towns exceeding 1,000 in population
- Political state of 10,000 in population
- Full-time craft specialists
- Full-time government specialists
- Social stratification or slavery
- Subsistence economy based on food production
- Intersocietal trade

#### a fundamental question
given the biological similarities and the cultural diversity of human beings, how differently are they likely to behave under analogous circumstances? There is no consensus.  


#### what happens when civilizations encounter others?  ( civilizations Contact, “intercivilizational encounters”, clash of civilizations)
 it is only one way of representing intercivilizational encounters, which do, after all, run the gamut from conflict and conquest to coexisting interconnection to fusion.  




#### how long do civilizations last?
 two primary modes of reckoning our future:
- extrapolation from our present circumstances;  
- projection based on our knowledge of the paths of civilizations which came before us.  

 Central to both (above modes) are our deepest hopes and fears about human nature, and our beliefs about the tools we create and the societies we make.

And today, humanity does have powers previously known only to the gods of legend, and gathers more each day. Guesses about where this will lead are frequently based on ideas about human nature, themselves often rooted in ideas about biological and social evolution. They play off our tendencies towards aggression, competition, and revenge against our predilections for altruisim, cooperation, and reciprocity, and make much of the fact that we have “stone age minds and the might of the gods”.  
“the very people who are the most evolutionarily unsuited to living in a technologically advanced world are most likely to predominate in the corridors of power,”


#### interesting things
 - Cities in a nation have an intriguing tendency to follow a log-normal distribution, whereby the second-largest city’s population is half the size of the largest city’s population, and the third-largest city’s population is one-third the size, etc
- it is a challenge to recognize our cultural assumptions and biases.   
- the “clash of civilizations” is not real, but an artifact of a way of thinking about what a civilization is; it is more fruitful to think of a “mobile labyrinth of civilizations, all of them caught up in the modern transmutation, but each of them possessing specific legacies and resources that can be activated in inventive ways”. ...... The history of exploration on Earth has shown that often, people find largely what they expect to find. ...... We all have a remarkable capacity to slot our observations into preconceived frameworks, and act accordingly. In other words, our intellectual models of contact **generate** history as surely as they recount it.  
- our deep ambivalence about technology.  
- To best use Earth cases in relation to cosmic questions, we need to understand the limits of the data, the methods used to study them, the reasons for which they have been studied, and the narratives and ideological positions they support. Without this kind of awareness, we run the risk of importing biased, oversimplified factoids and theories into the study of culture in the cosmos, and missing out on real discoveries.  