## _The Fourth Turning_ v.s. _Generations_  

### _Generations_:  
- the social moments:  
	- Outer-Driven (to celebrate the outer splendor of man’s power over nature)  
	- Awakening  
	- Inner-Driven (to glorify the inner fire of God’s power over man)  
	- Crisis  
- four archetypes:  
	- Idealist  
	- Reactive  
	- Civic  
	- Adaptive  
- how many cycles generations studied:  
	- 6 cycles, 18 generations  
	- Reformation  
	- Colonial  
	- Revolutionary  
	- Civil War  
	- Great Power  
	- Millennial  
- 18 generations are supported with history data

### _The Fourth Turning_:  
- the four turnings(saecular seasons):  
	- High  
	- Awakening  
	- Unraveling  
	- Crisis  
- four archetypes:  
	- Prophet  
	- Nomad  
	- Hero  
	- Artist  
- how many cycles generations studied:  
	- 7 cycles, 24 generations(Arthurian Generation –Millennial Generation)  
	- Late Medieval (1435-1487) (Hero, Artist)  
	- Reformation (1487-1594)  
	- New World (1594-1704)  
	- Revolutionary (1704-1794)  
	- Civil War (1794-1865)  
	- Great Power (1865-1946)  
	- Millennial (1946-2026?)  
- add more items in “Moods of the Four Turnings”  
- focus on World War II  

### DEFINITIONS:  
- saeculum: a unit of time, roughly 80~100 years  
- generation: a common collective persona. the aggregate of all people (born over roughly the span of a phase of life) who share a common location in history.  
- turning: a social mood that changes each time the generational archetypes enter a new constellation. Each turning is roughly the length of a phase of life.
- rhythms:  
One beats to the length of a long human life–saeculum. The other rhythm beats to the four phases of a human life, each about twenty years or so in length–generation.  

The limiting length of an active life cycle is one of civilization’s great constants: In the time of Moses, it was eighty to a hundred years, and it still is,  

Americans’ chronic failure to grasp the seasonality of history explains why the consensus forecasts about the national direction usually turn out so wrong.

### four archetypes:  
- A Prophet generation is born during a High.  
- A Nomad generation is born during an Awakening.  
- A Hero generation is born during an Unraveling.  
- An Artist generation is born during a Crisis.  

### the four turnings of the saeculum (growth, maturation, entropy, and destruction):  
- High:    
	an upbeat era of strengthening institutions and weakening individualism, when a new civic order implants and the old values regime decays.  
- Awakening:    
	a passionate era of spiritual upheaval, when the civic order comes under attack from a new values regime.  
- Unraveling:    
	a downcast era of strengthening individualism and weakening institutions, when the old civic order decays and the new values regime implants.  
- Crisis:    
	a decisive era of secular upheaval, when the values regime propels the replacement of the old civic order with a new one.

The Fourth Turning is history’s great discontinuity. It ends one epoch and begins another.

##  
Around the year 2005, a sudden spark will catalyze a Crisis mood.  
By the 2020s, America could become a society that is good, by today’s standards, and also one that works.  


### three ways of thinking about time: 
- chaotic, 
- cyclical, 
- linear  

## CH2  
Romans always distinguished between a civil saeculum (a strict 100-year unit of time) and a natural saeculum (the stuff of life, history, and imperial destiny).

Each cycle is represented by a circle, symbolizing perfect and unbreakable recurrence.

Two particular symbols of circular time are nearly universal. One is the looping serpent, … another is the traditional circle dance.  

Some ancient religions deified the number four.  

Each circle of time has a great moment of discontinuity.  

### All over the world, time’s rite of passage required three steps:  
- First, rituals of kenosis (emptying)—fasting, sacrificing, or scapegoating—purified the community of sins committed in the last circle and thereby allowed a new circle to begin.  
- The second step was a liminal, chaotic phase in which the old circle was dead but a new circle not yet born. In this phase, all rules were breakable: The dead could awaken, insults go unpunished, and the social order be inverted,  
- The third step required rituals of plurosis (filling)—feasting, celebrating, and marrying—to propel the new circle to a happy and creative beginning.

Each circle requires that time be restarted, at the moment of each creation.  
Each circle is presumed to repeat itself, in the same sequence, over a period of similar length.  

### which wheel has dominated the others as a marker in people’s personal and social lives?  
There are many cycles of time, each with a different periodicity…This prompts the question: Through the millennia, which wheel has dominated the others as a marker in people’s personal and social lives? As a society modernizes, however, one circle gradually emerges as paramount over all the others: This is the circle of the natural human life span—-the saeculum.    

### Why should the saeculum be so special?    
- the natural life span is probably the only circle that mankind can neither avoid nor alter.  
- as modern people exercise their freedom to reshape their natural and social environment, often in efforts to escape circular time, their innovative energy typically reflects their own life-cycle experiences.The life span plays a dominant role in the rhythm of history precisely when modern society has largely abandoned cyclical time in favor of linear time.

Crisis <–> A “revitalization movement,” a deliberate, organized, conscious effort by members of a society to construct a more satisfying culture.

### The phases of the modern American life cycle:  
- Childhood (pueritia, ages 0-20);  
	social role: growth (receiving nurture, acquiring values)  
- Young Adulthood (iuventus, ages 21-41);  
	social role: vitality (serving institutions, testing values)  
- Midlife (virilitas, ages 42-62);  
	social role: power (managing institutions, applying values)  
- Elderhood (senectus, ages 63-83);  
	social role: leadership (leading institutions, transferring values)  
- Late Elderhood (ages 84+);  
	social role: dependence (receiving comfort from institutions, remembering values)

### What happens as a Great Event and its echoes fade with the passage of time?  
In a traditional society, nothing. In a modern society, however, new Great Events keep occurring, and with great regularity. These are the solstices of the saeculum: Crises and Awakenings. Every generation has thus been shaped by either a Crisis or an Awakening

### why haven’t people always known about the connection between generations and history?  
If the connection between generations and history is so powerful, why haven’t people always known about it?  
People have. Yet in the ancient world, the connection was blurred by a confusion between family lineages and peer groups. Few traditional societies bothered to clarify matters because, being organized around family tribes, there was little need for a distinction: Among elites, each new marriage implied a new social generation. large generational differences did not often arise in a traditional setting.  
With the arrival of modernity, this changed. At about the same time that Europeans began to talk self-consciously about centuries, they also began to talk explicitly about peer groups.  

This Euro-American experience confirms that the faster a society progresses, the more persistently generational issues seem to keep springing up. the more modern a society thinks itself, the more resistant its people become to legitimizing generational change as an idea.

While modernity is about rational progress toward the future, generations stand as reminders of how much people remain tied to the subconscious vestiges of their past.

### a generation’s property:  
- its common location;  
- its common beliefs and behavior;  
- its common perceived membership.(how a generation defines itself, a popular consensus about which birth cohorts belong together. Perceived membership gives a generation a sense of destiny. A generation can collectively choose its destiny.)

### 隔代气质相反的现象：  
How else could young heroes emerge, if not in response to the worldly impotence of self-absorbed elder prophets? How else could young prophets emerge, if not in response to the spiritual complacency of hubristic elder heroes? This in turn requires that each generation exert a dominant formative influence on people who are two phases of life younger, that is, on the second younger generation…. This critical cross-cycle relationship is just what we see in most societies. It arises because a new child generation gathers its first impressions about the world just as a new midlife generation gains control of the institutions that surround a child.Even though *a child’s biological parents will be distributed about equally over the two prior generations* (because generations average about twenty-one years in length), the older parental group has the dominant role.  
Intentionally or not, most parents enter midlife trying to raise a new generation whose collective persona will complement their own

#### 隔代气质相反的结果：  
A key consequence of these cross-cycle shadow relationships is a recurring pattern that lies at the heart of the saeculum: an oscillation between them.  

#### 隔代气质相反，可以解释气质的顺序：  
These powerful cross-cycle phenomena explain why myths always depict the archetypes in one fixed order, the only order that is possible in the seasons of time: Hero to Artist to Prophet to Nomad. Recurring in this order, the four archetypes produce four possible generational constellations.  
all living things develop toward a destination contrary to the form in which they first present themselves.  
Value orientations do not change much during a generation’s life time, Committed during its early stages, a generation most often carried its value commitments into the grave.  

## CH4  
### The Origin of the American Cycle  
American Cycle可回溯至欧洲文艺复兴(the late 1400s):    
The self-sustaining cycle of archetypes originated at the very moment that the world made its enduring break with cyclical time and tradition. This happened in Western Europe during the last quarter of the fifteenth century. This Renaissance marked the true Western threshold into modern history.

### the birth of modernity = the Renaissance + the Reformation,   
解释：   
The Reformation redefined the search for moral conviction, a search which no longer interested worldly clerics and rulers, in terms of principles discernible by each person alone.By clearing away the intermediaries between the individual and God, the Reformation gave birth to an entirely modern definition of faith and conscience.

Where the Renaissance shattered and reassembled the medieval secular order, the Reformation did likewise with the medieval religious order. Where the Renaissance redefined historical time as worldly progress toward happiness, the Reformation redefined it as spiritual progress toward salvation.

- the Renaissance generation:   
Botticelli, da Vinci, and Bramante; Christopher Columbus, Amerigo Vespucci, and Vasco da Gama;  

- the Reformation generation:  
Martin Luther, John Calvin, Ulrich Zwingli, William Tyndale, Charles V of Spain, Ignatius Loyola

America’s very existence as a favored destination for migrants the world over has played a crucial role in the emergence of the generation as a unit of history. In early modern Europe, meaningful membership in generations was limited to elites—that is, to those who were free to break from tradition and redefine the social roles of whatever phase of life they occupied.

### Prophets:   
best remembered for their coming-of-age passion and for their principled elder stewardship. ... increasingly indulged as children, they become increasingly protective as parents.  
endowments: vision, values, and religion  
best-known leaders: John Winthrop and William Berkeley, Samuel Adams and Benjamin Franklin, James Polk and Abraham Lincoln, and Herbert Hoover and Franklin Roosevelt. These have been principled moralists, summoners of human sacrifice, wagers of righteous wars. Early in life, none saw combat in uniform; late in life, most came to be revered more for their inspiring words than for their grand deeds

### Nomads:   
best remembered for their rising-adult years of hell raising and for their midlife years of hands-on, get-it-done leadership

### Heroes:   
best remembered for their collective coming-of-age triumphs and for their hubristic elder achievements

### Artists:   
best remembered for their quiet years of rising adulthood and during their midlife years of flexible, consensus-building leadership

Archetypes in American History

## how every organism, through its development, both stays the same and yet transforms into its opposite? 
( In certain respects, a generation always retains its persona of youth; in other ways, it expresses that persona very differently in each successive phase of life)

A turning is an era with a characteristic social mood, a new twist on how people feel about themselves and their nation. It results from the aging of the generational constellation.

social cycle: growth, maturation, entropy, and death (and rebirth).

ARCHETYPES AND TURNINGS

The First Turning  
The Second Turning  
The Third Turning  
The Fourth Turning

### What would history be like if the saeculum did not exist?  
- In chaotic time, history would bear no pattern. Any effort to chart it would list columns and rows that describe anything—and therefore nothing. Society would zigzag aimlessly. At any time, it could accelerate, stop, reverse course, or come to an end.  
- In linear time, there would be no turnings, just segments along one directional path of progress. Each twenty-year segment would produce more of everything. On a chart, every cell in any given row would read just like the one before, except with a higher multiplier. …There would be no apogee, no leveling, no correction.Eventually, America would veer totally out of control along some bizarre centrifugal path.  
- In cyclical time, a society always evolves.  

Rhythms in History  
In 1969, young sociologist named Peter Harris. Harris reached a striking conclusion: Over three centuries of American history, a wide variety of social indicators have always turned an abrupt corner every twenty-two years or so. Emerging out of reams of archival evidence, this insistent pattern compelled Harris to rethink the standard-issue linearism of his academic specialty—and ultimately prompted him to switch fields. (He is now a history professor at Temple University.)

social indicators: birth rate, marriage age, wage growth, social mobility, political activism  

related indicators: the founding of Utopian communes,

### other cycles:  
- Politics (<–>the private interest) (Schlesinger and Burnham cycles)  
The public energy eras overlap largely with Awakening and Crisis turnings,  
Crises require a dramatic reassertion of public energy to fulfill the need for social survival. Awakenings require a dramatic reassertion of public energy to fulfill the need for social expression .  
- Foreign Affairs (Klingberg’s cycle)  
America’s response depends on whether the prevailing mood is ticking toward “introversion”(Awakenings and Crises) or tocking toward “extraversion.”(Highs and Unravelings). a subsequent era of extraversion is supposed to last until 2014.  
- Economy (income and class equality) (K-Cycles)  
the long-term performance of market economies peaks occur near the ends of Highs and Unravelings, and troughs occur near the ends of Awakenings and Crises.  
During a High, wage and productivity growth is typically smooth and very rapid. government plays an obtrusive planning and regulatory role.The rules of the game encourage saving, favor the young, and protect organized producers (monopolies, trusts, guilds, unions). it promotes income and class equality,  
During an Awakening, a soaring economy hits at least one spectacular bust. the popular consensus underlying this public role begins to disintegrate. it reduce income and class equality.  
During an Unraveling, economic activity again accelerates, but now the growth is unbalanced and fitful. public control recedes, while en-trepreneurship, risk taking, and the creative destruction of the market prevail. Meanwhile, the rules of the game encourage dissaving, favor the old, and protect individual consumers. It promotes income and class inequality.  
During a Crisis, the economy is rocked by some sequential combination of panic, depression, inflation, war, and public regimentation. Near the end of a Crisis, a healthy economy is reborn. a new popular consensus emerges. it reduces income and class inequality.  
- Family and Society  
(- Feminism)  
Awakening, Feminism, as a popular movement, bursts on the scene  
During an Unraveling, the gap between acceptable gender roles shrinks to its narrowest point.  
The efficacy of masculine power (and feminine morality) is reidealized during a Crisis.  
During a High, the gap between acceptable gender roles grows to its widest point, after which the cycle repeats.


Prophet generations always include impassioned women.  
Nomad young women have displayed some variant of the “gargonne” look that hides sexual differences, In midlife, Nomads to expand gender differences,  
Hero generations favor a rational paragon of leadership, which reasserts the public-private division of sexual labor.  
Artist young women have flaunted the hoops and beehives that accentuate sexual differences. In midlife, Artists to shrink gender differences,  
- Population  
Artist generations , baby bust  
High, rise in birthrates  
Awakenings (when Nomads are born) and Unravelings (when Heroes are born) show a less pronounced bust-and-boom pattern  
(- Immigration to America)  
to climb in an Awakening,  
peak in an Unraveling,  
fall during a Crisis  
- Social Disorder  
Rates of crime and worries about social disorder rise during Awakenings, reach a cyclical peak during Unravelings, and then fall sharply during Crises.  
- Culture(inner and outer ideals)(musical styles, architecture, fashion)  
A Crisis totally alters the social framework for the expression of thought and feeling.  
In a High, the culture optimistically reflects the public consensus . New currents(cultures) arise only on the fringe, where they subtly and unthreateningly begin to undermine the consensus  
Awakening, the civic order feels secure and prosperous enough to enable a new culture to erupt. New norms, styles, and directions first assault and then firmly implant upon the post-Crisis order.  
Unraveling, the new culture flourishes, splinters, and diversifies.  
When a new Crisis hits, the culture is cleansed, censored, and harnessed to new public goals.  
While every turning can lay claim to cultural innovation, some shine more in certain media than in others. (music, literature)  
- society’s response to Accidents and Anomalies(stock crash, the sneak attack on Pearl Harbor, Watergate burglary, invention of the microcomputer). Do these new technologies really change us, or do they just give us precisely what we want when we want it?  
most political assassination attempts in U.S. history have in fact happened during Awakenings.  
High-era wars…  
Awakening-era wars….  
Unraveling-era wars…  
Crisis-era wars…  

### Does the rhythm of the saeculum make a major war unavoidable? 
No one knows…History teaches only that whatever wars do happen always reflect the mood of the current turning.Wars in a Fourth Turning find the broadest possible definition and are fought to unambiguous outcomes. This suggests that, had the Japanese not attacked Pearl Harbor, the United States would have found some other provocation to declare total war against the Axis powers.

Today, archetypal constellations all around the world show striking similarities.

The saeculum is not an entirely stable social dynamic, and for a very simple reason: Total stability is beyond the reach of any human system. the saeculum cannot determine the quality, good or bad, of history’s endings.

## CH9  
Crisis morphology:  
A Crisis era begins with a catalyst—-a startling event (or sequence of events) that produces a sudden shift in mood.  
Once catalyzed, a society achieves a regeneracy—-a new counter-entropy that reunifies and reenergizes civic life.  
The regenerated society propels toward a climax—-a crucial moment that confirms the death of the old order and birth of the new.  
The climax culminates in a resolution—-a triumphant or tragic conclusion that separates the winners from losers, resolves the big public questions, and establishes the new order.

In every prior Fourth Turning, the catalyst was foreseeable but the climax was not.

Earlier chapters explained how Crisis eras shape generations; now you see how generations shape Crises. This explains the underlying link between the cycles of history and the rhythms of the saeculum.

While all generational transitions are important to create a Fourth Turning constellation, the aging of the Prophet is critical. A Crisis catalyst occurs shortly after the old Prophet archetype reaches its apex of societal leadership, when its inclinations are *least* checked by others. A regeneracy comes as the Prophet abandons any idea of deferral or retreat and binds the society to a Crisis course. A climax occurs when the Prophet expends its last burst of passion, just before descending rapidly from power. A resolution comes, with the Prophet’s symbolic assistance, at a time when the Nomad is asserting full control.

As visionary Prophets replace Artists in elderhood, they push to resolve ever-deepening moral choices, setting the stage for the secular goals of the young.  
As pragmatic Nomads replace Prophets in midlife, they apply toughness and resolution to defend society while safeguarding the interests of the young.  
As teamworking Heroes replace Nomads in young adulthood, they challenge the political failure of elder-led crusades, fueling a societywide secular crisis.  
As Artists replace the Heroes in childhood, they are overprotected at a time of political convulsion and adult self-sacrifice.  

## CH10 
Sometime around the year 2005, perhaps a few years before or after, America will enter the Fourth Turning.  
In retrospect, the spark might seem as ominous as a financial crash, as ordinary as a national election, or as trivial as a Tea Party.  
Recall that a Crisis catalyst involves scenarios distinctly imaginable eight or ten years in advance. Based on recent Unraveling-era trends, the following circa-2005 scenarios might seem plausible:  
Beset by a fiscal crisis, a state lays claim to its residents’ federal tax monies. Declaring this an act of secession, the president obtains a federal injunction. The governor refuses to back down. Federal marshals enforce the court order. Similar tax rebellions spring up in other states. Treasury bill auctions are suspended. Militia violence breaks out. Cyberterrorists destroy IRS databases. U.S. special forces are put on alert. Demands issue for a new Constitutional Convention.  
A global terrorist group blows up an aircraft and announces it possesses portable nuclear weapons. The United States and its allies launch a preemptive strike. The terrorists threaten to retaliate against an American city. Congress declares war and authorizes unlimited house-to-house searches. Opponents charge that the president concocted the emergency for political purposes. A nationwide strike is declared. Foreign capital flees the U.S.  
An impasse over the federal budget reaches a stalemate. The president and Congress both refuse to back down, triggering a near-total government shutdown. The president declares emergency powers. Congress rescinds his authority. Dollar and bond prices plummet. The president threatens to stop Social Security checks. Congress refuses to raise the debt ceiling. Default looms. Wall Street panics.  
The Centers for Disease Control and Prevention announce the spread of a new communicable virus. The disease reaches densely populated areas, killing some. Congress enacts mandatory quarantine measures. The president orders the National Guard to throw prophylactic cordons around unsafe neighborhoods. Mayors resist. Urban gangs battle suburban militias. Calls mount for the president to declare martial law.  
Growing anarchy throughout the former Soviet republics prompts Russia to conduct training exercises around its borders. Lithuania erupts in civil war. Negotiations break down. U.S. diplomats are captured and publicly taunted. The president airlifts troops to rescue them and orders ships into the Black Sea. Iran declares its alliance with Russia. Gold and oil prices soar. Congress debates restoring the draft.

## Images
![](img/1.png)  
![](img/2.png)  
![](img/3.png)  
![](img/4.png)  
![](img/5.png)  
![](img/6.png)  

