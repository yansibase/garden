
_Evolution Science and Ethics in the Third Millennium - Challenges and Choices for Humankind_  
2018   
by Robert Cliquet, Dragana Avramov


## CH1

### 1.1.1 The Darwinian Revolution
When speaking about evolutionism, one has to distinguish between evolution as phenomenon and the evolutionary mechanism as the explanation of that phenomenon.
GRI + LR --> SE + V --> NS + T --> BI
(GRI: geometrical ratio of increase; 
LR: limited resources; 
SE: struggle for existence; 
V: variation;
NS: natural selection; 
T: time; 
BI: biological improvement.)

### 1.1.2 The Modern Evolutionary Synthesis
the formulation of the Hardy-Weinberg Law. a simple equation which shows the incidence and intergenerational transmission of genes and genotypes in populations: this forms the basis of the mathematical modelling of the whole evolutionary process.

Second Darwinian Revolution-->evolutionary psychology, sociobiology.

A fundamental proposition of evolutionary psychology is that the mechanisms of our social cognition were adaptations to the hunter-gatherer culture of the Pleistocene past. This implies that our brain, with its basic mental content-specific cognitive mechanisms, is not adapted to the present environment of high population density and social complexity that we experience in modern culture.

#### modernisation's three main stages: 
- (1) the pacification process as a consequence of state organisation at the emergence of the agricultural era; 
- (2) the civilising process in Renaissance times; and 
- (3) the humanisation revolution in the wake of the Enlightenment.

The divergence between the human genetic predispositions requires cultural adaptation via technological interventions and value changes, because the natural evolutionary mechanism is unable to produce the necessary genetic adaptations quickly enough.

government([0, 5years]) < Futurologists([5, 50years]) < Evolution scientists ([50, 1000years]) < Bio-anthropologists([1000years, 1000000years])

This book looks at ecological, biological and cultural developments in the coming decades up to the end of twenty-first century without losing sight of a longer-term evolutionary perspective of this millennium.  

## CH2
meme: Cultural mutations, new ideas.  
Human morality evolved to contribute solving adaptive problems and achieving adaptive goals. Moral systems are cultural instruments that serve the same goal as biological organ systems, namely to promote ontogenetic and phylogenetic adaptation.  

Well-known examples of complex moral innovations of great social importance were the in-group transcendent norms of early Christianity and Enlightenment humanism.

### Important differences between genetic and cultural mutations:  
 - genetic mutations are, as far as is known, purely chance phenomena, meaning that they are probabilistic rather than deterministic in nature, whilst cultural mutations are more often the consequence of non-chance events.
 - genetic mutations can only be transmitted via biological parents, whilst cultural mutations can also be transmitted by non-parents. This is the reason why cultural change can progress so much faster than biological evolution.

cultural change occurs on the basis of both Darwinism and Lamarckism.

### co-evolution between biological and cultural mutants:  
 - moral ideas cannot be produced if the required brain capacity to produce them and the sensitivity to accept them are not available.  
 - cultural innovations may change the direction or strength of selective processes on biological predispositions facilitating moral behaviour.  

The strongly investing sex in offspring produces fewer descendants than the weakly investing sex. The strong investors will, consequently, develop a qualitative or K-strategy in order to ensure that each offspring produced has maximal opportunities for survival.   

reproductive strategies --> mating strategies  
male-male competition + female choice  
 
moral virtues are sexually attractive  

They conclude that simple reciprocal interactions, let alone kin selection,  would not suffice to explain the origin of the specifically human morality  

- altruism: behaviour that involves a cost to the actor’s survival or reproduction, 
- mutualism: (selfish) cooperation and mutual benefit for the actors involved. 
Mutually beneficial social behaviour is not only less costly than altruistic behaviour, but it is also less sensitive to cheating. 

reciprocal altruistic behaviour mainly relies on partner control. 
mutualism is based on assortative partner choice.

Social coercion of conspecifics, resulting in intra-species parasitism, is a unique phenomenon for the human species. In the animal kingdom slavery prevails among some ant species, but the enslaving only concerns other species, not the same one.

### three major foundations of human social life: 
kin selection,   
reciprocity selection,   
social coercion,  

Values and norms can be expressed through three channels: moral rules, religious rules, laws

### morality v.s. morals:
 - morality: a system of attitudes, standards, and/or behaviour by which humans evaluate certain forms of behaviour as good and other forms of behaviour as bad. (道德体系/道德体制？)
 - morals: rules about what people ought to do and what they ought not to do, in particular regarding interpersonal and social relations.（道德规范？）

moral philosophers and theologians --> abstract morality
Evolutionists --> behavioural morality (morality as a form of behaviour)

Evolutionary ethics: how morality is in one way or another related to the biological evolution of humankind, and consequently to its present biological constitution.

### evolutionary ethics deals with 3 issues:
- (1) to what degree do human moral sentiments as biological drives have an evolutionary foundation and, consequently, a genetic and neurological basis;  
- (2) to what degree does biological evolution, via natural selection (or other evolutionary mechanisms), also contribute to the development of moral values and norms embedded in culture;  
- (3) can moral values and norms in turn change the evolutionary course?  

## Two major approaches:  
- The descriptive approach, 
- The prescriptive (or imperative) approach.

### three major episodes in the development of evolutionary ethics theory:
- (1)nineteenth century, 
- (2)1930–1940s, 
- (3)1960–1970s

p54 there is no purpose in evolution, only a direction, a trajectory—the line of evolutionary progress that can serve as a guide in formulating man’s purpose for the future

p57 moral instincts and moral values have a strong, albeit not a unique, evolutionary foundation,

p58 traditional evolutionary ethics ~ competitive ethic, 
the newer evolutionary ethics(Darwinian ethics) ~ cooperative ethic

p59 moral values are the expressions of natural human drives aiming at the common good of society.

### 3 major biological bases (needs & drives) of morality: 
- 1. individual ontogenetic development, (2.2.2.1, CH6)
   drive for survival in general, thermoregulation, sleep, respiration, need for nutrition, physical safety, health care, family care, social bonding, mobility, resource acquisition, and habitat and territorial security; anguage development, creative and aesthetic pleasure; ntellectual understanding (including spirituality, religiosity and morality).
   --(cause)--> (a),(b) 
   (a) the innate sense of having the opportunity for individual self-realisation(e.g. aspirations such as survival, reproduction, freedom, welfare, well-being, and happiness)
   (b) the desire to implement basic values and norms usually grouped in modern times under the heading of human rights.
 - 2. sociality (living in groups) (2.2.2.2, CH7)
   nepotism (altruism toward closest kin); reciprocal altruism between non-relatives; mutualism; in-group favouritism and out-group enmity; male bonding; social dominance and submissive forms of behaviour; egalitarianism; punishment of criminals, cheaters and free-riders.
 - 3. reproduction(the intergenerational transmission of genes (and memes)) (2.2.2.3, CH8)
   development of sexual identity; sexual mating; love; female choice; male-male competition; sexual jealousy; incest avoidance; childbearing, childcare and education.

many scholars consider human supersociality as the primary source of our moral nature

###  human morality includes 2 interrelated phenomena:  (p61)  
- (1) The biological (neurological) basis of innate moral sentiments and the capacity for reasoning, also in the domain of morality, ultimately determined by genes;
- (2) The cultural values and norms (ethical codes), resulting from cultural creative processes and social learning and internalisation, but ultimately influenced or driven by innate moral sentiments on the one hand, and the capacity for moral reasoning and judging on the other hand.


### human morality v.s. hominisation process (p62)  
![](img/human-morality-vs-hominisation-process.webp)


### biological changes in the hominisation process(caused the development of human morality):
- (1) The shift from instinctive to conscious behaviour in hominin evolution;(2.2.3.1)  
    a bipedal gait, large brain hemispheres --cause--> instinctual behaviour to  conscious behaviour --cause--> an ever increasing degree of learned behaviour --cause--> more complex forms of social organisation --cause--> cultural regulatory systems(value and norm systems) --cause--> higher brain capacity
- (2) The increasing prematurity of hominin offspring during the hominisation process;(2.2.3.2)  
    --cause--> big-brained human new-born to pass through the female pelvis at birth, and to keep the foetal metabolic demands in balance with the mother’s metabolic capacity
- (3) The development of human sociality beyond the family.(2.2.3.3)  
    brain's 2 characterstics --cause--> Family and more general kinship relations --cause--> tribes and nations --cause--> abstract ideas about morality

### Direct adaptation or indirect exaptation
By stating that values and norms are a biological adaptation (or exaptation) the authors mean that they are phenomena deriving from certain biological needs,  

Compared to the duration of the great apes, the human pregnancy should last 21 months.  

very young children: prosocial inclinations/preferences (helping behaviour, sensitivity to fairness, indirect reciprocity and cooperative interaction) are intuitive and widespread.   
adults: social cooperation occurs on an intuitive basis and is perceived as emotionally rewarding.  

the increasing need for prosocial forms of behaviour --cause--> selective processes --cause--> the development of moral sentiments and the capacity for moral learning&reasoning became embedded in the human genome and made our brain function accordingly.

### basic emotions:   
pleasure, surprise, pain, fear, disgust, anger  

prosocial behaviour --cause--> social emotions (e.g. love, pride, embarrassment, guilt, shame, envy, and jealousy)  
Moral behaviour had a positive effect on reproductive fitness, because human groups with many altruists were more successful in dealing with the challenges of survival, adaptation, well-being, and competition with other human groups.

### four primitive prosocial dispositions:   
deference, self-control, altruism and cooperation

### 3 major stages in the evolution of morality: 
- (1) a biosocial stage; (linguistic capacity)
- (2) a cultural-religious stage; (8000–10,000 years ago, tribes-->chiefdoms-->states in the agrarian era. the moral systems founded on religion were characterised by the re-establishment of stronger,)
- (3) a cultural-scientific stage.(scientific revolution, Enlightenment. two features: moral individualism, moral universalism)

### What types of direct genetic knowledge about moral sentiments or behaviour are available?:
- (1) pedigree analysis of monogenic or chromosomal variants which are clearly linked to particular, usually pathological, conditions;
- (2) heritability estimates measuring the degree to which within-population variance in particular forms of moral sentiments or behaviour is determined by genetic factors, environmental factors, and genetic-environmental interaction and covariance; 
- (3) molecular genetics of particular sentiments or forms of behaviour relevant to morality.

the degree to which behavioural differences between individuals are influenced by genetic factors.
it cannot be expected that direct relations between genes and components of moral behaviour can be detected.  
molecular personality genetics

### two neurological pathways involved in the development of morality:
- (1) The neurological basis determining an innate moral sense;
- (2) The neurological capacity for moral learning and reasoning.

### which brain structures are involved in the production of evolved emotional and rational capabilities for moral behaviour?
progress in neurobiology 398 has allowed exploration and understanding of the neural bases of some of the most distinctive moral behavioural attributes of the human species

neuroethics

In fact, all of the major dimensions of morality are related to the activity of one or more brain areas.  
Morality-related brain structures and functions are not immutable over the life course of an individual.  
environmental factors—internal as well as external—can influence brain functions related to moral behaviour. 435 Responsible parenting stimulates earlier conscience development. 436 Early experiences result in better or worse equipped brains for moral life.  

### how to explain...the existence or persistence of a variation in personality characteristics and/or moral codes that can, in particular circumstances, result in diverse forms of (im)moral behaviour?

### big five dimensions of human personality:
- extraversion, 
- neuroticism, 
- openness to experience, 
- conscientiousness, 
- agreeableness


## CH3
The Golden Rule(ethic of reciprocity): one should treat others as one would like others to treat oneself. p104 . one should not do unto others what you do not want them to do to you. p271. 
 
religions and religiosity are part of our biological evolutionary and cultural historical heritage, which are adaptations—or exaptations—to pre-scientific living conditions.

The essential feature of religion is the belief in a supernatural agent or power that created the universe, explains its existence and meaning, and often imposes a moral code according to which humans should behave.

### five major attributes to religious systems: 
- (1) belief in a postulated supernatural agent; 
- (2) groups of people identifying themselves as sharing deeply held beliefs; 
- (3) presence of costly or painful sacrifices as commitment signals to the group; 
- (4) belief in gods and other postulated supernatural agents implying how people should behave; 
- (5) supernatural agents can be induced by prayers, donations, and sacrifices to intervene on behalf of mortal petitioners.

Religion: (a more general definition, from _The creation of God_(2010, by Pinxten R)  
Religions can be understood as a particular way of dealing with the Whole, or of reaching for a wholeness which transcends any individual in time and space, even in substance. In religious activities and utterances people express their cognitive, emotional and evaluative relations vis-à-vis others, animals, plants, the earth and the celestial phenomena.  

### two major meanings of the concept of God:
 - a personal God (=theism) is an anthropomorphic, omniscient, omnipotent, omnibenevolent, and merciful, supernatural being that created not only the universe but also life, including human life on our planet;
 - an impersonal God (=deism) is divine essence who created the universe, but does not intervene in people’s personal lives.


The concept of God is used here in the sense that organised religions such as Judaism, Christianity and Islam conceived it in their basic scriptures

### Religiosity: 
various aspects of religious activity, dedication, and belief.

### advanced countries v.s. developing countries
advanced countries ~ strong religious/ideological diversity amongst their populations.   
developing countries ~ much more limited religious/ideological diversity amongst their populations.

### religious people: 
 - show higher degrees of agreeableness and conscientiousness, 
 - are more sensitive to traditional and conservative values,  
 - are less inclined towards novelty/open-mindedness, 
 - are less prone to values indicating openness to change and self-enhancement.

a striking negative relation between degree of religiosity and educational level.  
a clear negative correlation between religiosity and cognitive ability.  

poorer nations: religiosity prevails most strongly
developed countries: secularisation

### Spirituality: 
a predisposition, partially embedded in the human genetic endowment, for experiencing self-transcendence.

### religious v.s. spiritual v.s. non-religious
![](img/religious-vs-spiritual-vs-non-religious.webp)

spirituality has a stronger genetic component in variability than religiousness, which is primarily transmitted through learning processes  

Spiritual experiences can be independent of religiosity  
Spiritually driven people also differ from religious people, for instance, in showing more compassionate and altruistic behaviour.   
Religion is an institutional characteristic; religiosity and spirituality are personal characteristics.  
religiosity and spirituality can be strongly influenced by environmental factors.  
in comparison to spirituality, religiosity appears to be more susceptible to memetic than genetic factors.  
eligion/religiosity results from an interaction of specific cultural, economic and ecological living conditions and the evolved human mind  

a positive relationship between degree of religiosity and moral behaviour  
children from religious families were less likely to share with others than were children from non-religious families;   
a religious upbringing was also associated with more punitive tendencies in response to anti-social behaviour  

morality has no positive connection with religion  
morality may have a positive connection with happiness indices. (high moral standards is found in many strongly irreligious populations in societies(score very high on happiness indices) )  

the basic needs of human nature (and its evolution) --> the development of morality in more complex social groups --> the religious consecration and imposition in large agrarian societies  

natural selection in our highly socialised species --> moral principles  

wisdom, courage, humanity, justice, temperance, and transcendence: these are personality traits that are universally considered to be moral virtues  

religion may positively contribute to the development of such moral virtues  

### 3.3 Origin and Evolution of Religion/Religiosity/Spirituality
biological predisposition to religiosity and religious memes emerged and proliferated as a function of biological evolutionary needs related to survival and reproduction.

#### 3.3.1 Earliest Signs of Religion/Religiosity/Spirituality
cannibalism, skull preservation, Ceremonial burials

### major stages in the social evolution of religion:
 - kinship-based religions / tribal religions / folk religions:
   (animistic in orientation. to strengthen bonding among members of the group, to promote the community’s survival.)
    - shamanic religions (= individualistic cult institutions + shamanic cult institutions)
    - communal religions (= shamanic religions + communal cult institutions),
 - organised world religions:
   (occurred with the emergence of agrarian-pastoral stage. to solve the social problems&conflicts that occurred as small societies based on kinship evolved into larger societies, composed of more numerous and less closely related people. to promote broader forms of social cooperation beyond narrow kin-based relations. environmental duress also appears to contribute to the development of such beliefs. to deal with the challenges provoked by the new living conditions in order to promote the cultural, political, demographic and biological success of their communities.)
    - polytheistic religions (= communal religions + ecclesiastical cult institutions + worshipping many gods)
    - monotheistic religions (= communal religions + ecclesiastical cult institutions + worshipping one god)
      (millennium BCE - the Axial Age. 原因： the massive increase in the scale of warfare(~~ socially disruptive and psychologically anxiety-producing phenomena(iron weapons, urbanisation) --> decrease people’s attachments to close kin and other social intimates) --> need for a substitute attachment figure in the person of a compassionate, all-powerful and loving God --> aim to resolve or adapt to the challenges&crises)

#### two major predictors of the change of tribal to organised religion: 
the development of agriculture, the invention of writing. 93

#### 2 major groups of religions: 
 - Eastern religions 
 - Mediterranean religions.  

their ethical prescriptions/strivings show much more similarity, for instance, in commending moral virtues such as reciprocal altruism, enlightenment, austerity, familial duty, loyalty, humaneness, honesty, truthfulness, humility.

Abrahamic 104 religions (Judaism, Christianity and Islam)
hose religions or at least two of them—Christianity (particularly Catholicism) and Islam—play currently an active role in slowing down some aspects of the modernisation process in many countries.

### why Christians became the only one of the numerous new religious sects in the late Roman Empire?
the Christian love message agreed very well, particularly in the socially disruptive environment in which it emerged, with human innate predispositions

### Islam
The Qur’an’s main message is to believe in the monotheistic, almighty and all-knowing, but unpredictable and capricious Allah in need of worship. The Qur’an is largely influenced and referenced by the Judeo-Christian scriptures.  
Its moral commandments largely reflect the male dominated social power relations in the ancient agrarian-pastoral, tribal-structured Arabian society.  
Muhammad 159 must have been a very charismatic, intelligent and shrewd man who understood very well how to appeal to his followers with his divinely inspired revelations. Historically, it appears that evolution towards family-transcending morality required stronger coercive imposition and punishment.  
Qur’an ....has more similarities with the Old Testament than with the quite innovative teachings of Christianity in the New Testament as expressed in the Sermon on the Mount.  
It may be argued that Islam’s theology contains a number of features and controversies that make it difficult for Islamic believers and their societies to fully contribute to and participate in the scientific and moral innovations of modern times.  

### 启蒙运动的重要性：
Present-day countries where Islam is the predominant religion have not undergone their own Reformation or Enlightenment. As a consequence the scientific and moral benefits of modernity are difficult to achieve, such as freedom of thought and speech, the acceptance of ideological and political pluralism and tolerance, the separation of religion and state, the creation and dissemination of scientific knowledge (e.g. concerning evolution science), the realisation of sexual equality, the right to individual self-realisation.  

In pluralistic environments....Muslims often present their faith as one of peace and tolerance. When one reads carefully the Islamic scriptures 179 or consults the national legislations of some Islamic countries, a totally different picture emerges.  

Philosophers have amply shown the logical incompatibility between the alleged multiple attributes of God and, in particular, the inconsistency between those attributes and the existence of evil.  

Martin M, Monnier R (2003) The impossibility of God. Prometheus Books, Amherst

### heritability-factor
![](img/heritability-factor.webp)  
Hence, a variation in individual religiosity should not be at all surprising.

significant relations between personality characteristics related to self-transcendence (and spirituality) and some genes that play a role in the physiology of several neurotransmitters in the brain.

### whether religious behaviour results from cognitive processes or from emotional predispositions?
a link between measures of empathic concern and religious or spiritual belief. In contrast, non-believers may have personality profiles that are more conducive to analytic thinking, which is, in turn, more associated with a naturalistic/materialistic worldview.  
associations between neurological processes and religious-mystical experiences.  
Certain pathological conditions (temporal lobe epilepsy, Parkinson’s disease, schizophrenia, head injuries) can influence religious attitudes and behaviours.  
various environmental factors have also been found to induce heightened levels of religiosity—hallucinogenic drugs, high mountains (associated with acute and chronic hypoxia), 228 and hot deserts.   

sacred values (religious/secular nature/inspiration) have been found to be associated with increased activation of several brain regions. sacred values affect behaviour through processing rights and wrongs (deontological imperatives) and not through evaluating costs and benefits (utilitarian concerns).  

neurotheology  

the modular architecture of the brain,..the mind consists of a set of discrete and functionally specialised problem-solving modules, each one related to the management of specific adaptive problems,  

### two mental tools are thought to be of great importance in mental processes related to beliefs about supernatural agents:
 - the Agency Detection Device (ADD), which detects the presence and activities of other beings around us, 
 - the Theory of Mind Mechanism (ToMM), 240 which consists of the ability to attribute mental states to others and to interpret their intentions.

### how and why beliefs in supernatural agents are so resilient?
hominin evolution --> powerful cognitive systems --> The capacity to develop such beliefs(god concepts are extremely easy to acquire and transmit.) ---> beliefs in gods seem to be quite ‘natural’.  

mainstream neurologists are of the view that there is no neurological argumentation for accepting such thing as a soul. Human mental life, including spiritual life, is a product of neurochemical and electrical activity of the brain.  

p120  
In other words, religiosity/religions must have favoured differential survival and reproduction  

### which proximate advantages spirituality/religiosity and religious beliefs/religions produced favourable survival and reproductive effects in the pre-scientific environment?
 - proximate advantages at the individual level:
    (1) understanding/explaining the facts of life and death;
    (2) mastering the events of life and death.
 - proximate advantages at the societal level:
    (1) promoting social cohesion and solidarity (defence against competing groups/populations, or conquering others’ territories and resources) 
    (2) reinforcing the social dominance of the leadership.

increasing brain capacity--->interest/fascination/wonder about the facts of nature, and the causes of life and death----?>an increased intelligence, a stronger self-conscious awareness.

religions/religious beliefs ---> relieve our existential fears and anxieties, to comfort us in a contingent, bewildering, threatening and dangerous world, to alleviate pains of disease and other forms of misery, and to fight the fear of and terror about the absurdity of death.

### what has the religious mysticism and myth-formation about life and death to do with evolutionary survival and reproduction?
the solutions to cope with matters of life and death---> diminishing or eliminating all sorts of life anxieties, and might also have provided people with positive feelings such as optimism, altruism, action-oriented motivation, self-sacrifice. ~~ improved mental and physical health----> survival and reproduction 

Religious ruling:  
 - death, 
 - health,
 - self-oriented innate drives, 
 - sexual and reproductive behaviour.

### three concerns that dominate the sexual and reproductive rulings in Abrahamic religions: 
- (1) facilitate males, with their strong, regular ejaculation pressures, sexual accessibility to the precious source of female bodies; 
- (2) protect the transmission of male genes against those of potential competitors; 
- (3) promote the spreading of the faithful’s genes and memes.

Hence, the memetic religious rulings about sexual and reproductive behaviour are ultimately strongly genetically oriented and explain the reproductive and evolutionary success of those belief systems.

### three major incentives for the religious control of sexual and reproductive behaviour:
 - guaranteeing males accessibility to female bodies, 
 - protecting males against cuckoldry, 
 - spreading the faithful’s memes and genes

All these above are the ultimate causes for women’s profound social oppression in human evolution and history.  
Traditionally, both in matters of sex (subjects for men’s sexual gratification) and reproduction(producers and carers of offspring), religions see women as the precious resource   


### (从繁殖角度)女性受压迫的原因：   
(compared with men) women's reproduce cost is high (because the pregenance duration is long) --> women is precious resource for reproduction --> men compete for more reprodution resource --> Religious ruling for men to protecting their reproduction resource --> women is oppressed  


126
most people are intuitively very obedient to authority.  
However, the degree to which authority is blindly obeyed varies cross-culturally.  
It helped individuals to understand incomprehensible facts of nature in a simple way.  
- From a biological evolutionary viewpoint:  
	in the pre-scientific era, religious-magic systems had to be considered as vehicles for the development, justification, imposition and transmission of ethical systems of vital importance for developing and transmitting human life.  
- From a psychological as well as a social point of view:  
	in the pre-scientific era, religious-magic were a ‘natural security system’.  

### Today, religions have some persisting advantages:
 - at the individual level:
    - be very satisfactory for coping with the anxieties of misfortune and death. to give them a meaning to life and death. 
    - to help poor-eduated people to behave morally, and motivate them to be socially cooperative members of their society
    - Deeply religious people may enjoy the considerable emotional and cognitive pleasurability of their religiosity/spirituality. 
    - Increased religiosity is associated with increased agreeableness and benevolence.
 - at the religious in-group level:
 - at the societal level:

Historically, science arose from religious and theological thinking. 419 Both try to understand/explain life and death, and both try to control life and death, but their methods (and sometimes also their ends) are completely different.  
Religion and science are the products of reflective thought.   

Conceptually and methodologically, religion and science are fundamentally different and irreconcilable: religions draw and justify their worldview and moral rulings from alleged, albeit unproven or ungrounded, divine revelations, whilst science builds its knowledge on empirical and measurable observation, corroboration, experimental testing, hypothesis and theory formulation, following the laws of logic. Moreover, science changes or refines its theories, explanations and predictions as soon as new evidence contradicts or complements earlier findings or views. Religions are constrained by their Holy Scripts. (科学理论追求变化，宗教不追求变化)

## CH4
### two ideological phenomena of a more general nature:
 - secularisation
 - atheism

### Liberalism:  It challenged the authority of absolutist rulers. ndividual emancipation and rights, equality of opportunities, freedom of thought and enterprise.
 - Classical liberalism  
 - social liberalism   
 - neoliberalism.  

### Capitalism的缺点：
Economic activity is undoubtedly an essential factor in satisfying human basic needs: however, this should not be done using ever increasing growth rates and the maximisation of profits, but in a socially well balanced and environmentally sustainable way....Evolution is about the long-term differential reproduction of genes in an evolving but sustainable environment, and not necessarily about maximising financial profits by means of (over)production of goods in an environmentally unsustainable way. In other words, the millionaire is not necessarily the fittest survivor.

### Socialism的缺点：
Overall, socialist ideology is strongly focused on societal structural matters, has lopsided and obsolete views about causes of individual development and the role of genetic factors in social relations, and often lacks a comprehensive view of societal processes or is commendably but one-sidedly focused on the least advantaged in society. It is characterised by a striking absence of long-term genetic and ecological concerns. In contrast, it rightly stresses the overwhelming importance of the social dimension and solidarity factor for harmonious individual development as well as for adequate relations between individuals and groups of individuals.   
From an evolutionary point of view, socialism lacks—just as its big opponent liberalism—a broad and holistic vision about the future needs of a further evolving humanity in a progressively modernising and globalising world.

### Feminism:
This first feminist wave: to achieving equal legal rights, general suffrage, equal access to higher education, and equal entry into the professions in order to achieve self-determination and independence.

#### The second feminist wave: 
women’s rights to bodily integrity and autonomy and reproductive rights, struggle against domestic violence, sexual harassment, sexual assault, sexual stereotypes, sexism and patriarchy in all spheres of life,and on more general societal problems such as armed conflict, environmental pollution, and Third World development.

#### a third feminist wave: 
not focused on privileged women  
All the major modern political emancipatory ideologies——eventually included principles and policies with a view of restoring—or perhaps better put, of establishing at last—social equity and equality between the two sexes.  

In modern societies, men still have or take advantage of some of their secondary sexual characteristics....heir biologically selected physical strength and drives for risky, agonistic and competitive behaviour, allowing them to acquire dominance socially, also towards women  

women are more religious than men,  
female behavioural characteristics: women have higher abilities to empathise; are more intuitive;
have a higher emotional intelligence; are more socially oriented, more friendly and more care taking; are better in emotional bonding; more easily recognise facial expressions and emotional overtones in others; have a more inward directed demeanour; display a more selective mating behaviour; have stronger parental impulses; display heightened social and personal concerns; experience more often anxiety; and show more interest in art, aesthetics, linguistics, and literature.  

selective processes --> the sex differences in brain structure and functioning  

male features adapted to hunting and war making, female characteristics more strongly focused on reproduction, nurturing and socialising.


new knowledge --> Ancient beliefs about innate female inferiority can no longer  be successfully sustained.  
fertility control --> it liberated women from virtually permanent reproductive functions and allowed for the establishment of new balance between reproductive, productive,  
The development and availability of safe and effective methods of birth control is of considerable importance in this respect, not only from a social but also from a psychological perspective.  

The transition from agrarian-pastoral to industrial and post-industrial economy --> ncreasing educational opportunities, female paid labour and financial independence.  
modern technology --> eroding the traditional male physical advantage with respect to muscular strength and speed.  

From an evolutionary point of view, the feminist movement must be seen very positively,  

From an evolutionary point of view, fascism clearly has nothing valid and promising to contribute to the ethical system, since it fundamentally ignores or challenges the mental and social potentialities of the human species as a whole.  

A significant feature of the humanist movement is that it is focused on the human species as a whole,  

four successive waves of modern humanism, originating around 1850, 1890, 1918 and 1945 respectively.

### modern humanist paradigm's six main characteristics: 
(1) a scientific method of inquiry;   
(2) a naturalistic cosmology;  
(3) a nontheistic orientation;   
(4) a commitment to naturalistic ethics;   
(5) a commitment to democratic forms of governance;    
(6) a commitment to international cooperation.  

Transhumanism aims to reach a posthuman stage in human evolution which would have characteristics such as higher-than-current intellectual heights; resistance to disease; increased longevity; unlimited youth and vigour; increased capacity for pleasure, love, artistic appreciation, and serenity; and experience novel states of consciousness. The transhuman is seen as an intermediate form between the present human and the posthuman.

Ecologism is a political ideology, it suggests that the biosphere and physical environment on our planet, which are of fundamental importance for our existence, should be the subject of moral concern and, hence, should be taken into account in policies.

A weakness of the ecological movement is that its policy proposals seldom have a sufficient systems-theoretical view that would replace the traditional approaches to environmental matters.

major subdivisions of ecologism: shallow environmentalism, deep ecologism
the interdependence between species is largely based upon competition between species

### 
- where does our admiration for nature come from? 
- Why does nature appeal so strongly to our aesthetic feelings or other mental needs? 
- How can the extension of our altruistic feelings and concerns to (some) other species be explained? 
- How did ecologism emerge in present-day modern culture?
The authors’ hypothesis would be that the psychological satisfaction and pleasure felt in the presence of a rich, diversified nature results from the evolved disposition and experience that such a nature guarantees the existence of rich and abundant (nutritional) resources. The aesthetic and altruist feelings towards (some) other species may also have developed in civilisations that acquired more knowledge about nature or reflected more thoroughly about the meaning and future of life on this planet.

it reminds us of the aesthetic or other mental pleasures a rich nature provides us with.

### Constraints of Secular Ideologies as Sources of Universal Morality:
- (1) their fragmented nature; 
     - liberalism emphasises the development of the individual and neglects collective impacts; 
     - socialism concentrates on societal issues often at the expense of individual self-realisation; 
     - feminism is mainly focused on gender issues; 
     - ecologism is environmentally and ecosystem oriented; 
     - nationalism is mainly, perhaps only, concerned about national in-group interests.
- (2) their short-term approach; 
- (3) their macro-level approach: They have wrongly assumed that efforts in devising and implementing well-intentioned systemic or structural solutions for societal challenges would automatically meet the human needs at the individual level.

modernity lacks a secular worldview that gives meaning and purpose to human existence and continuity.

## CH5
evolution-based general ethical goals for the future


evolution science can provide, in the context of a further progressing modernisation, a framework for elaborating global ethics to guide humanity to higher levels in the hominisation process.

### when defining ethical goals for the future of humankind, there are fundamentally two possible strategies one can choose: 
 - looking for directions in which way humanity should go。（本书倾向这个方法）
 - identifying directions in which we should not go. （Edward O. Wilson 选择这个方法的理由：Considering that science and technology are evolving at an extremely fast pace, it is quite precarious to make predictions and precepts about the longer-term future course humanity should take.）

### Why evolutionary ethics? 
three major reasons:    
- (1) the incomplete genetic programming of human development,requiring complementary, exo-somatic, cultural intervention in order to achieve optimal biological functionality;   
- (2) the social character of human nature, requiring cultural ruling to master inter-individual and inter-group relations;  
- (3) the cultural evolution of the human species, resulting in the modernisation process with its considerably increased capacity to master the environment and address traditional human adversities—ignorance, superstition, disease, hunger, war and environmental destruction.   

### argument: 
life is an intergenerational phenomenon and an evolving phenomenon.  
Knowledge about the evolutionary history of the hominins:  
 - allows the identification of the biological originality and specificity of present-day humankind.
 - allows the detection of the dominating trend(s) in the hominin evolution and extrapolates those trend(s) into the future and possibly identifies desirable future goals.
 - the identification of mismatches between our biological evolution and our cultural development. (This mismatch is evident for two reasons:  
	 - (1) biological maladaptations to the novel environment of modernity,   
	 - (2) some traditional moral codes and conventions are inappropriate to biosocial or biocultural challenges in modernity)

the evolutionary approach provides a standard against which human variability (and adaptability) can be measured and evaluated. 

evolutionary ethics makes it possible to deal with all of the major philosophical issues ethicists raise:
 - the question of intentionality (“how people ought to act toward one another”),
 - the authorative question (“why ought one be moral?”), 
 - the distributive question (“whose interests ought to be promoted?”), 
 - the substantive question (“which interests ought to be promoted?”).

any notion of a supernaturally inspired or determined ultimate design and purpose in life and human history has to be abandoned....humans have to define for ourselves the design and purpose we want to see in life and history.

### The ethical choices:  
(1) a further progressing hominisation;   
(2) a further progressing enlightened modernisation.  

### 8 ethical goals (evolution-based general ethical pursuits):
#### (2 prerequisites:)  
    1. ecological sustainability;  
    2. cultural progression of the modernisation process.  
#### (1 main aim:)  
    3. the phylogenetic enhancement of the hominisation process.  
#### (5 goals direved from the main aim:)  
    4. the ontogenetic development of human-specific potentialities;   
    5. the promotion of quality of life;   
    6. the promotion of equity;   
    7. the shift from competitive toward cooperative efforts  
    8. the promotion of universalism  

### 
The increased capacity for intervention in modern culture is characterised by the potential to gradually replace the major mechanisms of biological evolution by cultural mechanisms....With modernisation we witness a transition from trial and error to ‘intentional evolution’. This capacity for intervention is the basis of a new evolutionary worldview  

modernisation embraces tolerance toward variation and even deviance from mainstream cultural characteristics.

the more individualistically and religiously oriented United States   
the more social(ist)- and secular-oriented European Union,   
the more individualistically oriented West   
the more community-focused Far East.  

medical interventions and changes in reproductive behaviour in modern society seem to have a slight dysgenic effect. Some argue that the present course of human evolution is not going in a desirable direction. However, this is probably only a temporary consequence of a major shift in cultural change and its associated demographic regime. In the near future—namely in the course of this millennium --this dysgenic effect might be neutralised, if not reversed, by future improvements in genetic knowledge and genetic modification, and by the adaptation of behavioural norms to the new genetics and demographics.

### how to avoid evolutionary regression:
 - prevent culturally induced mutations  
 - restrict the reproductive behaviour of the carriers of unfavourable genes  

Evolutionary stabilisation would require major human interventions.  

### ways to enhance the hominisation process:
- (1) the enhancement of specific human characteristics through cultural influences upon ontogenetic development;   
- (2) change the genetic composition of the human species, eventually leading to a next stage in the evolution of the hominins.  

Edward O Wilson: We are about to abandon natural selection, the process that created us, in order to direct our own evolution by volitional selection   

The human drive to improve oneself is a fundamental characteristic of our species. The desire to acquire new capacities is as ancient as our species,—“a natural outgrowth of our human intelligence, curiosity and drive”....Whereas in pre-modern times, religions mostly channelled those aspirations into a heavenly hereafter, the Enlightenment, to start with Frances Bacon’s New Atlantis, 124 pushed these desires nto scientific discoveries and their applications in technology and societal organisation for improving our earthly welfare and well-being.

### Intervention:
 - cognitive abilities (including biological instruments of communication, memory, information-processing, reasoning),   
 - emotional personality characteristics that facilitate sociability, altruism, reciprocity, mutualism and cooperation,   
 - other desirable human attributes such as creativity, health, immunity to diseases, longevity, physical vigour (speed, strength, endurance), sexual arousal and orgasm, euphoria, and physical attractiveness.  

### two major evolutionary enhancement movements:
 - the eugenics movement 
    the improvement of the physical, mental and social capacities of the human species
 - the transhumanist movement. (The prefix ‘trans’ in transhumanist implies that humans should transcend their present-day capabilities.)  
    the ontogenetic enhancement of the human species.  
    the phylogenetic enhancement of the human species.  
    higher-than-current intellectual heights;   
    resistance to disease; increased longevity; unlimited youth and vigour; increased capacity for pleasure, love, 141 artistic appreciation, and serenity; experience of novel states of consciousness; and heightened capacity for sociability, etc.  

Homo erectus --> Homo sapiens sapiens--> Homo sapientior/the posthuman

In the mind of many transhumanists, individual freedom is considered to be of paramount importance for deciding about their own body and the characteristics of their children

Francis Crick: If we can get across to people that the idea that their children are not entirely their own business and that it is not a private matter, it would be an enormous step forward.

### two major strategies to enhance human genetic composition:
 - eugenic engineering   
 - behavioural changes in (differential) reproduction,  

### the new eugenics:
eugenic engineering. the (future) technological manipulation of genes  

### the old eugenics: 
traditional behavioural differentiation of reproduction  

the transhumanists’ idea is to change, by means of biotechnological interventions, the genes of our future children and to create so-called designer babies.  

transhumanist want to avoid any association with the dysgenism of the twentieth century authoritarian ideologies, in particular Nazism.  

The specificity of the transhumanist approach is that, in addition to the traditional educational and training efforts to develop moral behaviour, modern biomedical means would also be used to influence or change the underlying biology or genetics of moral behaviour, not only in the case of extreme socio- or psychopathologies but also in general. 169 Some of the transhumanist advocates of moral enhancement are even of the view that this type of enhancement should be a precondition for applying enhancements in other domains, particularly in the domain of cognitive capacities.

For example, it is known how our present cognitive capabilities are related to brain size, energy expenditure, body height, pelvis size, pregnancy duration, emotional personality characteristics, sociability, etc. （有资料可查吗？）

Evolutionary scientists also acknowledge the roughness, cruelty and defectiveness of the evolutionary system, which is based on chance events. It is proceeding at a very slow tempo, using and wasting enormous amounts of energy, and resulting in far-from-perfect adaptations, senescent degeneration, morbidity and mortality.

It is just not true that humans are born equal;... if we treat them equally, the result must be inequality in their actual position; ...thus, the only way to place them in an equal position would be to treat them differently.

...every student should have an equal opportunity to be treated unequally.

In conclusion, an evolutionarily-based ethic will, in the living conditions and opportunities of modernity, simultaneously have to positively value genetic diversity, favour—depending on the ecological, economic or cultural circumstances—particular genetic variants, enhance equity and equality of opportunities, and, in order to facilitate the social inclusion of disadvantaged population categories, develop adequate social protection systems.

Furthermore, the exponential increase in scientific knowledge, as well as the improvement of between-group communication by means of ICT (information and communication technologies), international educational and commercial exchange, travel and tourism, all together fundamentally undermines the in-group syndrome by breaking through group isolation and eroding two of its most basic breeding grounds ---- ignorance and prejudice.


## CH6
During hominisation the pace of the human growth process evolved, so that postnatal growth acceleration is followed by a period of interruption of growth acceleration in the time before adulthood. This postponement of maturation until puberty, controlled by the hypothalamus, relates to the long period that the human brain needs to become fully functional. During this period of pre-puberty maturation and socialisation the growing individual is still quite docile. From puberty onwards, an individual comes into sexual competition with other adults.

The evolutionary explanation of the occurrence of senescence is that ageing is caused by a decrease in the force of natural selection with increasing age. Selection against genes that manifest themselves early in the life course affect a larger number of individuals than selection against genes that reveal themselves at advanced age, when the number of survivors and their reproductive capacity are smaller. Genes with late detrimental consequences can accumulate and result in senescence among individuals who live sufficiently long enough. Senescence is the inevitable result of the fact that selection has a greater impact on genes that only affect survival or fertility early in life than genes whose effects are only manifest late in life.  
> 我的理解：与生存繁育相关的genes A，比如，吸引异性。与衰老相关的genes B，比如，糖尿病、高血压、健忘、阿兹海默症。进化选择能更有效地（正向）选择gene A，进化选择无法有效地（逆向）选择gene B。假设人无法衰老的话，那么随着年龄增长，人群里gene B的症状就越来越多，不利于整个人群的发展。

Evolutionary theory explains the increase of human lifespan as an adaptation related to the increase in brain size.

postponement of senescent processes <--- larger investment in somatic maintenance and repair <--- longer lifespan <--- lower fertility(larger birth interval & monoparous gestation) <---  longer maturation time <--- Larger brains ---> better environmental control -->  reduction of mortality

On the one hand, biological maturity now comes earlier in the life course due to the temporal growth acceleration; on the other hand, societal maturity takes more time due to increasing needs for schooling and training. This expanding gap between biological maturation and societal maturity is a typical example of the asynchronic biosocial development in modern culture, as biological-evolutionary adaptation is not fast enough to respond appropriately to cultural change over time.

Currently it continues to increase by approximately three months every year. 人的寿命增长速度  
the human species-specific lifespan is not very well known  

ageism: marginalisation or discrimination based on age, and especially prejudice against the elderly

The traditional categorisation of paid work and measurements of productivity need to be reconsidered—taking into consideration society as a whole and not only the enterprise/industry level. 

Avoiding morbidity and mortality at a younger age and allowing people to reach old age gives people the opportunity to fully develop their own genetic potentialities, and also creates the conditions for their parental and grandparental investment. It also prolongs the potential for the overall contribution to society by each individual.

As long as the duration of human biological maturation does not increase or other reproductive traits do not require a longer reproductive life phase, natural selection will not push toward a longer lifespan. From that viewpoint an additional prolongation of the potential lifespan seems to be a redundant, useless investment. However, modernity has fundamentally changed the natural rules of the evolutionary game:

Indeed, we should be well aware of the fact that individuals cannot evolve: only populations (species) evolve. Hence, achieving individual immortality might, in theory, mean the end of hominin evolution.

human offspring's long-term neediness --> enduring and intensive care of children -->
human sexual specificity(more enduring relations between the sex partners) --> longer paternal involvement and investment --> new male characters(less aggressive, more cooperative, caring, providing and protecting partner) & new female characters(concealed ovulation, large breasts, multiple erogenous zones, face to face sex interaction, shift from a cyclical toward a non-cyclical sexual readiness, capacity for orgasmic sex)

### sexual dimorphism in mind(mental aspects of sexual differentiation):
 - males are more oriented toward impersonal sexuality (sex dissociated from love) 
   (polygyny, having sexual intercourse more quickly, more often and with more partners)
 - females are more focused on personal sex (sex with carefully selected partner). 

promiscuous sexuality --> polygamy --> monogamy(for men, lower competition, higher cooperation)
agrarian economy --> dense population --> monogamy --> low risk of sexually transmitted  infections

For males, extra-pair mating is evolutionarily explained as a secondary mating strategy aimed at spreading genes over a larger number of partners. 
For females, the evolutionary advantage is thought to rely on obtaining an increase in quality of genetic offspring.

An important factor for understanding the sexually differential mating strategy in the human is the strongly age-linked sexual differences in the capacity to produce additional viable offspring. In the human species, the male’s reproductive capacity is huge and is, from puberty onward, relatively independent of age. In contrast, women’s fecundity is low and strongly time dependent. 

## two major aspects of love:
 - compassionate love (sexual desire) ~~ reproduction related mating system
 - companionate love (romantic love) ~~ the pair-bonding system that evolved as a function of raising slowly maturing offspring.  

Both are controlled by distinct patterns of brain activation. ref:    
- Diamond(2004) Emerging perspectives on distinctions between romantic love and sexual desire.
- Fisher(2005) Romantic love: an fMRI study of a neural mechanism for mate choice.
- Gonzaga(2006) Romantic love and sexual desire in close relationships.

The origin of love: the psychological foundation of the social bond that made, via increasing bi-parental care, an essential contribution to the survival of children.

The evolutionary origin of love is grounded in the cooperation between the sexes which is aimed at securing parental investment in offspring.

Sexual deviations occur in both sexes but most, if not all, are much more prevalent in men than in women. Hewitt has given a plausible evolutionary explanation: since men can fertilise several women, on average they are reproductively more disposable than women and can acquire traits that fulfil other functions. Women’s reproductive investment in each offspring is much higher, costly and makes her more vulnerable and selective in her reproductive strategies. 

homosexuality is maintained in the population at a relatively high frequency 129 which cannot only to be accounted for by recurrent chance mutations.  

Hunter-gatherer societies are, on average, characterised by relatively equal sex relations.  
the agrarian-pastoral cultural phase --> social inequalities of all kinds  

From an evolutionary point of view, a more basic cause for homophobia may be attributed to the fact that in order to maximise their inclusive fitness, people do have an interest in the sexual orientation of their offspring.

A possible longer-term consequence of the lifting of the taboo on homosexual relationships might be that the genes for same-sex preference may decrease in the gene pool, since the transmission of genes for homosexuality via (forced) heterosexual relations will be reduced.  

environmentally induced homosexual behaviour, for instance, related to early life experiences such as overprotective motherhood and authoritarian fatherhood,  

Moral norms have always been strongly directed towards regulating sexual relations ...  
At the proximate level, sex is one of the strongest competitive (and often also aggressive) drives in human behaviour and consequently it has to be ordered socially.   
At the ultimate level, sex is the instrument for reproductive behaviour and consequently for the (differential) transmission of genes—the pre-eminently most important factor at the intergenerational level.   

Modern, and in particular peaceful, societies with their millions of citizens are much less vulnerable from a reproductive point of view and, hence, can be much more relaxed in matters of sexual relations.  

Tolerance toward some sexual variants, such as homosexuality and childlessness, can be permitted but cannot be expected to become the norm.

sex equality and equity does not imply that women and men should perform, in equal numbers, exactly the same functions in society or should be equally present in all occupations.

there is one domain where sex equal numerical representation should be guaranteed and imposed at all levels, namely ethical and policy decision-making.  

Overall, the sex equality- and equity-oriented morality should concentrate on creating equal opportunities. This principle implies the establishment of sex-specific selective measures in many domains of social life, but especially in the domains of educational, occupational and retirement policies, taking into account the inevitably larger maternal investment in offspring.  

high potentialities for strenuous physical performance combined with lower cognitive abilities, or vice versa; exceptional artistic creativity combined with weaker health characteristics; high intelligence combined with low fertility  

the current historical moment can already be considered as the beginning of the transhuman civilisation.

### three principles (ethical objectives) relate to individual development: 
 - the ontogenetic development of human-specific potentialities,  
 - the promotion of quality of life,  
 - the promotion of equal opportunities.  

the fluctuations of body size in history show 207 that in poorer living conditions smaller body height has a higher survival value.

### The principle of equal opportunities consists of three complementary normative precepts:
- (1) equality in the distribution of basic goods and services within a society;   
- (2) equity in the distribution of surpluses beyond the provisioning of our basic needs according to merit;  
- (3) reciprocity in the contribution to the collective survival enterprise in accordance with our ability and performance.  

altruistic behaviour is clearly a strong incentive or condition for cooperative behaviour.

the strong biological predispositions for cooperation are, as an evolutionary result of those mechanisms, to be considered as one of the most specific features of the human species.

### three reasons why altruistic behaviour outcompeted selfish/competitive behaviour: 
- (1) it protects altruistic members from exploitation by the self-interested (shunning, ostracism and execution of free-riders);  
- (2) it favoured systems of socialisation that led individuals to internalise the norms that induce cooperation; 
- (3) it was an advantage in between-group competition for resources and survival that was, and remains, a decisive force in human evolutionary dynamics  

However, ... reciprocal or mutualistic behaviour is in fact, at least in its ultimate effects ... to be equated with genetically selfish behaviour. In other words, social cooperation, without excluding moderate forms of individual competition, fulfils the same functions as competition.

Evolutionary Causes of Antisocial Behaviour in particular circumstances they may have had some reproductive advantage and success and consequently have been intergenerationally transmitted. Most importantly, since most people have the ability to develop or display selfish as well as altruistic forms of behaviour, the combination of these predispositions...may tilt the adaptive strategy they choose in one direction or the other. Consequently, on an evolutionary scale, these elements may result in the simultaneous co-existence of different behavioural strategies that compete with each other.  

antisocial behaviour remains a minority phenomenon, simply because reproductively it is a less advantageous form of behaviour than cooperative behaviour in a social species and it is subject to negative selection.  

### antisocial behaviour's relatively high frequency, particularly among males. why?:
 - cheater (or ‘cad vs. dad’) theory of antisocial behaviour, 
 - the r/K theory of antisocial behaviour, 
 - the coincidental status-striving theory, 
 - the cuckoldry-fear theory.  

the common assumption: antisocial forms of behaviour produce, especially in particular living conditions, reproductive advantages for individuals who victimise others, allowing their own genetic predisposition to be transmitted to future generations, or prevent the reproduction of others.

psychopathy: the result of being born with abnormal emotional personality traits of congenital origin, 
sociopaths: the result of a failure to receive adequate socialisation due to weak or irresponsible parenthood or social deprivation in general.

psychopathy is a strategy of individuals who are unsuccessful in the normal competition for resources or mates, therefore they use deception and cheating to get them without reciprocating.

### modernity struggles with two fundamental problems regarding competitive behaviour.
 - the individual drive for outcompeting others in order to obtain or secure resources for self-actualisation and reproduction.
 - the specific economic and technological features of modernity that incite individuals, or groups of individuals, to express their urge for resource acquisition far beyond their individual or family needs

The human species is a social species, but its genetic predispositions for sociality have been largely selected as a function of social relations with few (and mostly closely related) people, and ultimately with the view to individual self-interest.

## CH7
### four major categories of group:
- (1) kin and family;  $\approx$ nepotism.  
- (2) social status; $\approx$ classism.  
- (3) race, ethnicity, worldview, or political conviction; $\approx$ racism, ethnocentrism, or ideological intolerance  
- (4) statehood. $\approx$ xenophobia.  

Nuclear family relations include two major components: partnership relations and parent-children relations—the latter descending as well as ascending.  

evolutionarily inspired scholars maintain that the origin of the human family is of a biological nature and that, even today, especially in modern culture, the only functions that keep families together remain biosocial in nature. 3 The family is a typical sociobiological group phenomenon.  

The human family is, very simply, the solution our hominid ancestor evolved over three to five million years to raise brainy, slow maturing, neotenic, highly dependent, and therefore, very costly (in terms of parental investment) babies  

Evolutionary science provides us with four theories from which specific hypotheses about mate preference and choice in an enduring partnership can be derived:   
 - ‘good genes’ theory, 
 - parental investment theory, 
 - reproductive value theory, 
 - paternity confidence theory.

Women’s age-specific natural fertility varies quite substantially: it increases in the first years after puberty, peaks in the mid twenties, and thereafter it gradually and increasingly declines with age, especially after 35 years of age.  

A clear distinction must be made between difference and inequality. Most of the time differences are products of nature (or culture).  

### four groups of differences in oopulations within current nation states:  
- (1) population genetic differences, often commonly referred to as racial differences;  
- (2) ethnic differences, characterised by differences in cultural traits such as language or customs;   
- (3) differences in worldview (‘Weltanschauung’, philosophy of life), including religious and various forms of non-religious beliefs such as atheism, agnosticism and freethinking;  
- (4) differences in political conviction or adherence.  

violent conflict played an important role in the evolution and history of humankind,   

In the course of the hominisation process, this intergroup competition for females resulted, by means of sexually selective 168 and group selective processes, 169 in the establishment of genetic predispositions to male bonding, in-group cooperation and favouritism, and out-group enmity: all of these were instrumental to intergroup conflict and warfare. In the further course of the hominin evolution, with its gradually increasing cultural complexity and numerical size of human populations, intergroup competition for females expanded to the increasing need for a greater variety and amount of other natural resources.  

altruism and cooperation may have been strongly driven by between-group conflict and warfare, or at least by group defence.  

In the gathering-hunting stage of human evolution there was a positive association between warfare and reproductive success, because mating and fitness benefits were worth individual lethal risks. 173 This was probably no longer the case in the agrarian-pastoral and industrial eras where warfare, fought by ever larger armies equipped with increasingly technological killing weapons, mainly had proximate benefits such as territorial expansion, economic power and resources acquisition. This gives less reproductive benefits for modern societies, where armies are recruited on the basis of physical and mental health features, knowledge-based opinion seems to tilt towards contraselective effects. War is dysgenic in modernity.  

Another limiting factor in the development of efficient intergovernmental bodies is the presence in world gatherings of a large number of non-democratically elected governments who defend the interests of authoritarian oligarchies, and governments that are politically inspired by ideologies that emerged and flourished in pre-modern cultural eras  

The increasing cooperation between states is embedded in four major features of modernity:   
(1) development of the ethics of democracy;   
(2) potential for generalised advantages of technological progress;   
(3) expansion of tools such as IT for dissemination of knowledge and ideologies;   
(4) balance of terror due to the development of weapons of mass destruction.  

The more democratic nations are, the less likely they will be disrupted by domestic violence and the less likely they will provoke international conflict or be involved in war.  

Altruism and parochialism may have a common evolutionary origin, but the two are not inseparable... parochialism need not be our destiny.  

The solution for improving the relations between states is very simple, although probably very difficult to realise. It is simple because there is only one solution, namely to increase international and intergovernmental cooperation; it is most difficult due to the tenacity of the in-group/out-group syndrome and the vested interests linked to it.  

## CH8
Differential genetic reproduction is the ultimate instrument for biological evolution.  

Fecundity = potential fertility: the number of live-born children that could be realised without the limiting effects of sexual taboos, late marriage, lactational amenorrhea, pathological sterility, contraception and induced abortion.  

hunter-gatherer and agrarian-pastoral eras: the observed completed fertility often averaged five to six live births.  

ertility increased in the agrarian-pastoral era 12 and, although mortality also increased,   

One of the important characteristics of modernisation is the demographic transition—the shift from high to low mortality and fertility levels.     
the beginning of the demographic transition was characterised by mortality control and continued high fertility, resulting in explosive population growth  

two demographic transitions in the modernisation process:  
 - the first demographic transition(the old industrial countries),   
 - the present demographic transition ( the developing world.)  

low fertility is a good adaptive strategy:  
- (1) in a long-term perspective,   
- (2) from a population or global point of view,   
- (3) in a novel context of modernity with its high consumption patterns and high standards of quality of life (including low mortality),  

by the end of this century it is expected to be back at the very low values it had during most of human prehistory.  

Well-known examples are the Anabaptists (Amish, Hutterites, and Mennonites) in North America who continue to reproduce at natural fertility levels (on average about 7 children per woman), and the Islamic immigrants in many European countries who have fertility rates which are double or triple that of the host population. However, the fertility rates of second and third generation descendants of immigrants tend to be much lower than those of the original immigrants.  

Human reproduction needs to be seen in the context of ethical goals for a further evolution of the hominisation process:     
- (1) the preservation of ecological sustainability;   
- (2) the cultural furthering of the modernisation process.  

From the point of view of the individual’s perception, ethical values do not extend beyond five or six generations....From an evolutionary perspective, human life has a temporal dimension that extends over a period of several million years and is characterised by a process of hominisation, the essential feature of which consists of increasing encephalisation, resulting in a growing capacity for controlling life and the environment  

facts:  
- (1) the present world population already has an ecological overshoot of 1.5 Earths, 
- (2) the developing regions in the world rightly want to acquire well-being levels comparable to those of the most advanced countries or regions 
- (3) the highest population growth rates exist in the least developed countries or regions.

In the hypothesis that Europe’s consumption pattern would further increase between 2007 and 2050, as it did between 1991 and 2006, a sustainable world population would amount to 1.03 billion people.   
In the hypothesis that the whole world would reach the developmental and consumption levels Europe had in 2007, the planet could support 2.5 billion people. These figures (1.3–2.5 billion) correspond quite well with other estimations, based on more sophisticated calculations available in the literature, which all range between 1 and 3 billion.   

A well-known and often heard alternative solution to the current ecological challenges is the reduction or change in the excessive production and consumption patterns in the developed world.

Quality of life includes:  
 - the nature of genes and gene combinations we inherit from our parents,
 - the way in which our life course develops under the influence of our genome
 - the environment with its material and immaterial components that we create for ourselves and our environment

In the long run, modern contraception will erase the genetic effect of the machismo of philandering males since extramarital affairs will no longer have reproductive effects.  

In many countries considerable proportions of highly educated or professional women remain childless.  

three major human qualities can be identified that are subject to eugenic action: cognitive abilities, emotional personality characteristics that facilitate altruism and sociability, and mental and physical health.  

Whereas the cloning of exceptionally talented individuals might have positive effects on the short-term biological and cultural future of humanity, this practice must be discommended at the population level because, when massively practiced, it would reduce biological variability and counteract the advantages of genetic recombination linked to sexual reproduction. 

more children in homes with better environmental opportunities.  

highly educated women often want more children than they actually have, but are confronted with deficit fertility due to their long studies, their high professional responsibilities, and their geographical mobility leading to the absence of grandparental, other family and social support.  

From an evolutionary-based ethical point of view, it is essential that in matters of genetics, not only the interests and well-being of the individual making choices but also the health and welfare at the population level are considered; but above all, attention should be given to the well-being of future generations.

## CH8

Indeed, Islamic countries struggle with three obstacles on their way to modernisation—philosophical, social and political.

a future diversity of worldviews and cultures could not be viable without a universal core ethic, built on the knowledge of the evolutionary sources of human variability and specificity.  

Global Ethical Foundation (1993), Declaration Toward a Global Ethic:  
“The world is in agony. The agony is so pervasive and urgent that we are compelled to name its manifestations so that the depth of this pain may be made clear. Peace eludes us—the planet is being destroyed—neighbors live in fear—women and men are estranged from each other—children die! This is abhorrent. We condemn the abuses of Earth’s ecosystems. We condemn the poverty that stifles life’s potential; the hunger that weakens the human body, the economic disparities that threaten so many families with ruin. We condemn the social disarray of the nations; the disregard for justice which pushes citizens to the margin; the anarchy overtaking our communities; and the insane death of children from violence. In particular we condemn aggression and hatred in the name of religion. But this agony need not be. It need not be because the basis for an ethic already exists. This ethic offers the possibility of a better individual and global order, and leads individuals away from despair and societies away from chaos. We are women and men who have embraced the precepts and practices of the world’s religions: We affirm that a common set of core values is found in the teachings of the religions, and that these form the basis of a global ethic. We affirm that this truth is already known, but yet to be lived in heart and action. We affirm that there is an irrevocable, unconditional norm for all areas of life, for families and communities, for races, nations, and religions. There already exist ancient guidelines for human behavior which are found in the teachings of the religions of the world and which are the condition for a sustainable world order. We declare: We are interdependent. Each of us depends on the well-being of the whole, and so we have respect for the community of living beings, for people, animals, and plants, and for the preservation of Earth, the air, water and soil. Parliament of the World’s Religions Declaration Toward a Global Ethic, page 2 We take individual responsibility for all we do. All our decisions, actions, and failures to act have consequences. We must treat others as we wish others to treat us. We make a commitment to respect life and dignity, individuality and diversity, so that every person is treated humanely, without exception. We must have patience and acceptance. We must be able to forgive, learning from the past but never allowing ourselves to be enslaved by memories of hate. Opening our hearts to one another, we must sink our narrow differences for the cause of the world community, practicing a culture of solidarity and relatedness. We consider humankind our family. We must strive to be kind and generous. We must not live for ourselves alone, but should also serve others, never forgetting the children, the aged, the poor, the suffering, the disabled, the refugees, and the lonely. No person should ever be considered or treated as a second-class citizen, or be exploited in any way whatsoever. There should be equal partnership between men and women. We must not commit any kind of sexual immorality. We must put behind us all forms of domination or abuse. We commit ourselves to a culture of non-violence, respect, justice, and peace. We shall not oppress, injure, torture, or kill other human beings, forsaking violence as a means of settling differences. We must strive for a just social and economic order, in which everyone has an equal chance to reach full potential as a human being. We must speak and act truthfully and with compassion, dealing fairly with all, and avoiding prejudice and hatred. We must not steal. We must move beyond the dominance of greed for power, prestige, money, and consumption to make a just and peaceful world. Earth cannot be changed for the better unless the consciousness of individuals is changed first. We pledge to increase our awareness by disciplining our minds, by meditation, by prayer, or by positive thinking. Without risk and a readiness to sacrifice there can be no fundamental change in our situation. Therefore we commit ourselves to this global ethic, to understanding one another, and to socially beneficial, peace-fostering, and nature-friendly ways of life. We invite all people, whether religious or not, to do the same.”
