_Generations_  
1992  
William Strauss, Neil Howe  





### THE GENERATIONS  
- The Colonial Cycle
	- Puritans  
	- Cavaliers  
	- Glorious    
	- Enlighteners  
- The Revolutionary Cycle  
	- Awakeners   
	- Liberty     
	- Republicans     
	- Compromisers     
- The Civil War Cycle   
	- Transcendentals   
	- Gilded   
	- Progressives   
- The Great Power Cycle   
	- Missionaries   
	- Lost   
	- G.I.s   
	- Silent   
- The Millennial Cycle   
	- Boomers   
	- 13ers   
	- Millennial   


### Assumption:  
the future replays the past (if the future replays the past, so too must the past anticipate the future)  

### A premise:  
each generation tries to redefine the social role of older phases of life as it matures through them (p72)  

### one important condition for four generational types recurring in fixed order: 
that society resolves with reasonable success each secular crisis that it encounters. When this condition does not hold, the cycle experiences an interruption—in effect, skipping a beat. (p74)

### 
we give equal weight to each phase of life (p109)

### 
Part I(CH2~CH6), CH12, Appendix A 都是在讲研究方法。  


### Our purposes:  
- to dispel the illusion of generational sameness.  
- to explain how the underlying dynamic of generational change will determine which sorts of events are most likely.  


### new answers to old questions:  
 why ...great public emergencies in America seem to arrive every eighty or ninety years,
 why great spiritual upheavals arrive roughly halfway in between.

### Prediction   
What we do claim our cycle can predict is that, during the late 2010s and early 2020s, American generations will pass deep into a “Crisis Era” constellation and mood—and that, as a consequence, the nation’s public life will undergo a swift and possibly revolutionary transformation.  

### Generational Cycles的意义：
- In our view, the timeless dynamic of human relationships comes first and matters most. While others may describe the technology with which America will send a manned spacecraft to the planet Mars, for example, we tell you something else: When America’s leaders and voters will want this flight to happen; why; which generation will fly it; and how the nation will feel about it at the time and afterward.

- What we do insist is that generations offer an important perspective on human events, from the great deeds of public leaders to the day-to-day lives of ordinary people.

- The generational cycle indeed raises important questions about when and how certain racial, ethnic, and women’s issues arise. (started during what we call an “Awakening’’ constellation of generational types. )

the widest gaps between acceptable male and female sex roles have taken place during an “Outer-Driven’’ constellation.

### terms
- generational cycle:  
	recurring four types of “peer personalities” patterns  
- peer personality:  
	a set of collective behavioral traits and attitudes that later expresses itself throughout a generation’s life cycle trajectory. Peer personality is produced during childhood and, especially, during the coming-of-age experiences.  
- cohort-group:   
	a group of all persons bom within a limited span of years. it stresses the  link between age and events,
- social moment:   
	critical event (e.g. [secular crises, spiritual awakenings])
- a social moment's cycle $\approx$ two phases of life (roughly 40 to 45 years)
- a generational “constellation,” has four generations: 
  - child,   
  - young adult,   
  - middle-aged,   
  - old.  
Note: coming of age separates youth from adulthood.  
- a generation $\approx$ 22 years  
- a generation: a cohort-group whose length approximates the span of a phase of life and whose boundaries are fixed by peer personality (p60)  



### 一些generation的特点：  
- the G.I.,(Civic?) has  trouble recognizing how other generations have personalities very different from your own.   
- the Silent, imagines being anything other than what you are.  
- the BOOMER, Idealist (narcissism,self-satisfaction)  (quite the reverse of Civic generations, typically exert their most decisive influence on history late in life.)  
- 13er, are dumb, greedy, and soulless.  is an ill-timed lifecycle.  

## 研究方法：  
- strip away gradual secular trends (rising living standards, improving technology, expanding population, shifting geography),
- recurring patterns in generational constellations and recurring types of historical events (crisis in American history:)
	- the colonial emergencies culminating in the Glorious Revolution of 1689; 
	- the American Revolution; 
	- the Civil War; 
	- the twin emergencies of the Great Depression and World War II.
	- America’s five great spiritual awakenings: from the Puritans’ “City on a Hill” in the 1630s to the Boomers’ “Consciousness Revolution” that began in the late 1960s .   

## CH1 People Moving Through Time



### Our theory of generations is two related theories:
1. an “age-location’’ perspective on history   
==>how events shape the personalities of different age groups differently according to their phase of life, 
how people retain those personality differences as they grow older.  
2. Generations come in cycles. 
history <---interaction(social moments)---> generations 


### generational types:
- Idealist (“dominant” in public life. redefining the inner world of values and culture)
- Reactive (“recessive” in public life, checking the excesses of their more powerful neighbors. as pragmatists, )
- Civic    (“dominant” in public life. rebuilding the outer world of technology and institutions)
- Adaptive (“recessive” in public life, checking the excesses of their more powerful neighbors. as ameliorators, )

Examples:  
spiritual awakening: Idealists(adulthood), Reactives(children)  
secular crisis: Civics(adulthood), Adaptives(children)  

### peer personality:
- Idealist:  
	attacking elder-built institutions  --->  retreating into self-absorbed remission --->   mature into uncompromising “Gray Champion’’ moralists
- Reactive:  
	bubble over with alienated risk-taking ---> mellow pragmatists 
- Civic:  
	are aggressive institution-founders  ---> stolid institution-defenders 
- Adaptive:  
	are elder-focused conformists  --->   junior-focused pluralists

### 
p35 social moments
correspondence between constellations and events, the average deviation is only 3~4 years.

p44 cohort, cohort-group
p45-47 three fallacies
p48: cohort-group's advantage: all its members, from birth on, always encounter the same national events, moods, and trends at similar ages.

p57 there is a third approach: to read history along the generational diagonal, 

## CH3 Belonging to a 4 ‘Generation”
### 
(p60) we CHOOSE to base the the length of a generational cohort-group on the length of a phase of life. we DEFINE life phases in terms of central social roles.  
 - ELDERHOOD (age 66-87).   
	 Central role: stewardship (supervising, mentoring, channeling endowments, passing on values)
 - MIDLIFE (age 44-65).  
	 Central role:  leadership (parenting, teaching, directing institutions, using values).
 - RISING ADULTHOOD (age 22-43).   
	 Central role: activity (working, starting families and livelihoods, serving institutions, testing values).
 - YOUTH (age 0-21).   
	 Central role: dependence (growing, learning, accepting protection and nurture, avoiding harm, acquiring values).
All we require is that each role be different and that the age borders for each role be well defined.  

### 
family relationships --> pattern --> 3 important corollaries:
 - A generation's parents (or children) are distributed over the two preceding (or two succeeding) generations;
 - A generation's early or “first-wave” cohorts are likely to have an earlier parent generation than late or “last-wave” cohorts;  
 - Each generation has an especially strong nurturing influence on the second succeeding generation .

- The first rule reflects the link between the age distribution of childbearing (with the average age of mothers and fathers typically ranging from 20 to 45) and the average length of successive generations (twenty-two years).
- The second rule, the distinction between what we call a generation’s “first wave” and “last wave,” helps us understand differences in the nurture received by early as opposed to late cohorts in any single generation. 
- the crucial nurturing relationships between generations two apart reflect the greater social influence of older over younger parents.  


### Peer Personality
use Peer Personality to identify one generation  

the attitudes of women and mothers toward their own sex roles and family roles are central to a generation’s peer personality.  

p64 A PEER PERSONALITY is a generational persona recognized and determined by (1)(2)(3):
- (1) common age location;  
Chronology: Common Age Location in History.  
To find boundaries between generations, we look closely at the history surrounding two moments: birth and coming of age.
- (2) common beliefs and behavior; and 
Attributes: Common Beliefs and Behavior
- (3) perceived membership in a common generation.  
Awareness: Perceived Membership in a Common Generation.  

A generation, like an individual, merges many different qualities, no one of which is definitive standing alone. But once all the evidence is assembled, we can build a persuasive case for identifying (by birthyear) eighteen generations over the course of American history. All Americans bom over the past four centuries have belonged to one or another of these generations.



## CH4 The Four-Part Cycle  
(How to identify recurring elements in these peer personalities)
p71  A SOCIAL MOMENT is an era, typically lasting about a decade, when people perceive that historic events are radically altering their social environment.

### two types of social moments: 
 - SECULAR CRISES, when society focuses on reordering the outer world of institutions and public behavior; and 
 - SPIRITUAL AWAKENINGS, when society focuses on changing the inner world of values and private behavior.

Social moments normally arrive in time intervals roughly separated by two phases of life (approximately 40~45 years), and they alternate in type between secular crises and spiritual awakenings.

In Appendix A, we explain in some detail how and why this timing of social moments tends to occur in nontraditional societies like America, and how it is linked to a cyclical creation of generations and generational types. At the heart of our explanation lies the premise that **each generation tries to redefine the social role of older phases of life as it matures through them**.   
The new social moment represents a reaction against the ossifying and dysfunctional roles forged by each generation during the earlier social moment. As a result, the new social moment will be opposite in type from the one that came before. 

- During social moments, DOMINANT generations are entering rising adulthood and elderhood.
- During social moments , RECESSIVE generations are entering youth and midlife.

(Since social moments normally arise as each generation is entering a phase of life, with each moment typically culminating when the generation has fully entered it, we show them appearing somewhat before each twenty-two-year interval)

In Figure 4-2 , we see the generational cycle. These four generational types recur in fixed order, given one important condition: that society resolves with reasonable success each secular crisis that it encounters. When this condition does not hold, the cycle experiences an interruption—in effect, skipping a beat...If a secular crisis weakens instead of strengthens the confidence of rising adults, then a Reactive generation can be followed by an Adaptive rather than a Civic.(1860s, Civil War)

- An AWAKENING ERA (Idealists coming of age) triggers cultural creativity and the emergence of new ideals, as institutions built around old values are challenged by the emergence of a spiritual awakening.

- In an INNER-DRIVEN ERA (Reactives coming of age), individualism flour-ishes, new ideals are cultivated in separate camps, confidence in institutions declines, and secular problems are deferred.

- A CRISIS ERA (Civics coming of age) opens with growing collective unity in the face of perceived social peril and culminates in a secular crisis in which danger is overcome and one set of new ideals triumphs.

- In an OUTER-DRIVEN ERA (Adaptives coming of age), society turns toward conformity and stability, triumphant ideals are secularized, and spiritual discontent is deferred.

 the two dominant types—Idealist and Civic—are key. Coming of age into rising adulthood, these two types recast society’s new “active” agenda, either from secular to spiritual or vice versa

p77 evens --> response --> social moment

Idealist generations are nurtured to burst forth spiritually upon coming of age. When they do, they awaken other generations along with them.

The generational cycle is deterministic only in its broadest outlines; it does not guarantee good or bad outcomes. 

Each generation has flaws, and each constellational mood comes with dangers. 

### 确定性 v.s. 不确定性
The cycle provides each generation with a location in history, a peer personality, and a set of possible scripts to follow. But it leaves each generation free to express either its better or its worse instincts, to choose a script that posterity may later read with gratitude or sorrow.


### America’s decisive moments of secular crisis:
- the Glorious Revolution of 1689, 
- the American Revolution, 
- the Civil War, and 
- the twin emergencies of the Great Depression and World War I

### Why we choose to start at 1580s? (p82) 
- no more than one hundred persons bom before 1584 came, settled, and survived more than five years—of whom perhaps only two or three dozen were lucky enough to find spouses and bear children. The 25,000 members of the Puritan Generation, on the other hand, consisted almost entirely of permanent settlers—of whom between 7,500 and 10,000 survived and bore children in the New World, often in large families. Quite simply, the numerical contrast between all pre-1584 cohorts and the Puritan Generation is overwhelming.
- The Puritan Generation also possesses all the striking attributes of an Idealist-type generation. 



- Colonial Cycle: 600,000 people
- Revolutionary Cycle: 8,000,000 people
- Civil War Cycle: 50,000,000 people
- Great Power Cycle: 200.000. 000 people
- Millennial Cycle:180.000. 000 people (340,000,000 projected by 2025)


In each of these periods of secular crisis, public institutions suddenly strengthened...During and shortly after these periods, leaders reshaped public institutions beyond earlier recognition. History turned, decisively.

The constellational pattern is unmistakable. In every secular crisis except the Civil War, America had old Idealists, midlife Reactives, rising-adult Civics, and child Adaptives. Picture the peer personalities of each generational type, at its respective phase of life:    
- Idealist elders providing principle and vision;  
- Reactive midlifers understanding how the real world functions, and leading accordingly;  
- Civic rising adults, smart and organized, doing their duty;  
- Adaptive youths emulating adults and making relatively few demands.  

This is the optimal generational lineup for overcoming social emergencies, attacking unsolved problems... Elders bring wisdom, midlifers savvy, rising adults cooperation, and children silence. The resolute order-givers are old, the dutiful order-takers young. This is a constellation of action, not reflection. People become pragmatic, community-oriented, and more risk-averse in public and private life. The distinctions between sex roles widen, and the protection of children grows to maximum intensity. The crisis leaves a heavy footprint on the remaining lifecycle of every generation then living—especially the Civic, who thereafter assume a hubris bom of triumph, a belief in community over self, and a collective confidence in their own achievements that their elders and juniors can never match.To propel the generational cycle in this manner, a secular crisis must end with triumph. 


### The Civil War Anomaly (p91)
- First, the crisis came early.  
- Second, the three adult generations allowed their more dangerous peer instincts to prevail.   
- Third, the crisis came to an untriumphant end.   
no rising generation emerged to fulfill the usual Civic role of building public institutions to realize the Transcendentals’ visions. Instead, the Gilded aged into a unique Reactive-Civic hybrid.

### Spiritual Awakenings (p92)
a spiritual awakening can "alter the world view of a whole people or culture."...each awakening episode was, in its own time, an update of the “individualistic, pietistic, perfectionist, millennarian ideology” which “has from time to time been variously defined and explained to meet changing experience and contingencies in our history.... And the moment is not essentially public or collective (though it may spark crowds, hysteria, and violence), but personal and individual. 

#### awakening的产生原因 (p93):
when a real-world threat triggers disciplined collective action and sudden institutional change, an awakening is driven by sudden value changes and a society-wide effort to recapture a feeling of spiritual authenticity. ...During the Reformation and Puritan Awakenings, these new “understandings” arose almost entirely in terms of religious dogma. 

Whereas a crisis empowers the rising-adult generation, an awakening endows it with a spiritual or ideological mission.

For an awakening cycle, picture the peer personalities of each generational type, at its respective phase of life:  
- Civic elders confidently running or overseeing institutions they once built around an earlier set of values;  
- Adaptive midlifers feeling pulled in competing directions by more powerful generations on either side;  
- Idealist rising adults experiencing spiritual conversion near the coming-of-age moment and cultivating implacable moral conviction;    
- Reactive youths too young to participate, left alone, urged to grow up quickly, and criticized as “bad.”  
This lineup of elder doers and rising-adult thinkers, rarely does well at large public undertakings.On the other hand, this constellation can generate great spiritual energy and unusual creativity in religion, letters, and the arts.  
During each Awakening era, we witness mounting     
 
### Reactive童年不幸的原因：（p98）
during Awakening eras, years of young-adult rapture, self-immersion, and attacks on elder-built culture. Home and hearth are assaulted, not exalted。 The generational constellations of these Awakening eras feature angry two-way dramas between Civic elders and Idealist rising adults, with Adaptives mediating between them. None of these older generations offers much comfort to society’s Reactive newcomers. （Conversely, during the Crisis eras at the other end of the cycle, the constellation produces the opposite form of nurture: the suffocating overprotection of Adaptive children.）  
#### 教育截然相反的原因：
Since the passage from youth to midlife takes a generation through two phases of life (half a cycle), midlife generations tend to raise the current generation of youth in a manner opposite to that in which they themselves were raised.
Reactives may well have their own laissez-faire upbringing in mind when they try to overprotect their Adaptive young. But they are also influenced in midlife by their exhausted and risk-averse outlook toward life in general and by the high priority that Crisis-era society places upon protecting the family at all cost. Opposite motives govern Adaptive parents. In part, their nurturing style reflects intended compensation for memories of their own dark-closet childhood, but midlife Adaptives are also enmeshed in a quest for personal liberation and are constantly reminded (by other Awakening-era generations) that everyone needs more social autonomy。

Childhood nurture plays a major role in shaping a generation’s peer personality. Even as children, peer groups begin to create attitudes and expectations (within themselves and among elders) that determine how they will later fulfill social roles... we can often observe how these attitudes and expectations later manifest themselves in adulthood.

### 为什么关注come of age? (p101)
Childhood nurture alone does not stamp a generation for life. To define its peer personality and fix its cohort boundaries, a generation needs further contact with history, especially as it comes of age into rising adulthood. 

### 
every social moment has ended near the first birthyear of a dominant-type generation.
### 
In describing lifecycles, we give equal weight to each phase of life (plus the critical coming-of-age experience between youth and rising adulthood). (p109)


“precisionism”: prayer, work, reading, spiritual diaries, and sporadic bursts of abstinence and political activism.

## Images  
![](img/project-0.png)  
![](img/project-1.png)  
![](img/project-2.png)  
![](img/project-3.png)  

[All in one image](img/project-all.png)  


## CH11 The Millennial Cycle 
sex revolution --> women freedom --> high divorce rate --> harm children  


## CH12 The Past as Prologue
p349 our cycle is perfectly compatible with the progress of civilization by any standard normally used to measure it—material, spiritual, social, or cultural.

to measure the American Dream by Civic milestones: public order, community purpose, friendly neighborhoods, dutiful families, benign science, and a rapidly ascending standard of living. Take those milestones, assess the trends of the past two or three decades, and reflect on what a linear view of history would conclude about America’s future. Now that’s pessimism. But a cycle offers hope. It suggests  that the standards by which Americans measure progress shift from one era to the next— from material to spiritual, from community improvement to self-perfection, from basic survival to civilized refinement—and then back again. 

p350 One lesson of the cycle is that every generational type has its own special vision of the American Dream. Each type can fare well or badly in fulfilling that vision. Likewise, each can leave gifts—or harms—to its heirs. We call these “generational endowments.”  

p350  the moods (culture, fashions, politics)  

### To identify a mood:（研究social mood的方法）(p352) 
1. strip away the cumulative shape of civilization that every constellation largely inherits from the past—such as affluence, technology, basic social mores and cultural norms, and established political institutions.
2. strip away the chance events and the passing fashions, language, and mannerisms

### Popular music支持了研究social mood的这种方法：
Popular music offers a lively illustration of how cyclical recurrences can be distinguished from linear trends. ... popular music has reflected the mood of each new era and the type of each new peer personality. Over the Great Power Cycle, the most memorable Missionary songs were spirituals and blues, soulful and angry; the Lost had jazz, improvised and naughty; the G.I. invented big-band swing, standardized and upbeat; and the Silent had folk and rock, subversive and infused with social conscience. So far in the Millennial cycle, that pattern has repeated—with Boomer music in wardseeking and tinged with fury (adding the “acid” and “Jesus” to rock) and 13er music punkish, prankish, and diverse.


### major social indicators: (p353)  
- Rates of substance abuse  
	Rates of substance abuse tend to rise steeply during an Awakening era, peak near its end, and then fall after last-wave Idealists finish coming of age (and first-wave Idealists enter midlife). ...young Idealists get most of the euphoria while society looks on indulgently, after which young Reactives get most of the blame when society begins cracking down.
- Fertility  
	For the Missionaries, the anomalous Civil War Cycle created a somewhat different pattern: the birthrate suddenly fell for war-baby first-wavers (bom from 1860 to 1865),
- Immigration  
	reflecting a different social consensus about pluralism and community.
- Economic growth  

(p355) Summary of the mood

(p357) The cycle of generational types: a paradigm
(p357) the Idealist lifecycle
(p359) the Reactive lifecycle
(p360) the Civic    lifecycle
(p362) the Adaptive lifecycle

### Generational Endowments (p367) 
generation’s awareness of death motivates generation’s endowment  
原因：  
Any work we leave undone in our lifetime can be completed later by another member of our group. But generations are different. Each has only a limited time to make its mark or otherwise keep its peace.

(people) have had trouble recognizing direct links between their own behavior and the progress of civilization. Thus when people make major sacrifices on behalf of the future, social scientists tend to puzzle over such behavior. ... however inadequately such motives explain the behavior of great social benefactors, such as an Andrew Carnegie or a Martin Luther King, Jr .... Just as age location connects our personal biographies with the broad currents of history, so do generations bridge the gap between personal and social goodwill. Whatever other reasons we might have to behave well toward the future, our generational membership prompts us to act now rather than later.

(p369) Just as a society’s vision of the future shifts with the constellational mood, so does the primary focus of its endowment activity. How it shifts depends on the phase-of-life positioning of peer personalities. 

the link between endowments and generational types:
- Throughout its lifecycle , each generation s endowment efforts are concentrated in areas closely connected to its peer personality.
As a generation matures, its endowment behavior becomes part of its peer personality
Just as each generation (and type) tends to stake out a matching endowment agenda, so do other generations come to defer to that generation (and type) for providing such endowments. 

Within any era, the most striking pattern is not so much the emergence of a new endowment activity (which is gradual), but the decline of an old activity (which is rapid). Since younger generations have grown accustomed to relying on an older generation to champion its own preferred endowment activity, they are unprepared to fill the gap when that older generation passes on. ...The implications are clear:
- In each era , the most noticeable endowment neglect or reversal is likely to occur in the endowment activity associated with the generation currently passing beyond elderhood.

The overlapping pattern between generations and endowment behavior leads to a final lifecycle observation:
- Each generation develops a lifelong endowment agenda pointing toward the endowment activity that society neglected or reversed during its youth.

Endowment behavior reflects a stabilizing dynamic intrinsic to the generational cycle, which ensures that no one dimension of progress is pushed too long—or lies dormant too long....This makes the cycle of generations a powerful force for rejuvenation, a balance wheel for human progress

(p381) If we plot a half cycle ahead from the Boom Awakening (and find the forty-fourth anniversaries of Woodstock and the Reagan Revolution), we project a crisis lasting from 2013 to 2024. If we plot a full cycle ahead from the last secular crisis (and find the eighty-eighth anniversaries of the FDR landslide and Pearl Harbor Day), we project a crisis lasting from 2020 to 2029. By either measure, the early 2020s appear fateful.

How old will each generation be when the crisis arrives? On average, history tells us, the crisis begins sixty-two years after the Idealists’ middle birthyear and nineteen years after the Civics’ middle birthyear. It peaks five or ten years later. 
What will precipitate this crisis? 
How significant will this crisis be?
What will the national mood be like? 
How will this crisis end?
What happens if the crisis comes early?

## CH14 The Beginning of History
The cycle teaches us that bad endings can take decades to build and in their early phases can be hard to foresee

===========================




# Vocabulary  
the generation's collective mind-set: 集体的思维定式（思维模式）   
collegial identity  
premises  
collegians  
passing mention of many subjects  
pluralism  
generational (and lifecycle) behavior/ change / dynamics  
pragmatist 实用主义者  
ameliorator 改良者  
recessive隐性的，后退的，逆行的  
liturgy  
midlifers,   
parental nurture, schooling, adult expectations, economic trends, cultural tone  
in this respect,   
not escape the eye of   
parenthood  
he use of chemical, germ, or nuclear weapons by small nations; revolution in Latin America; belligerent Third World fundamentalism; AIDS; the global warming trend; ozone depletion; exhaustion of fossil fuels; the abuse of high-tech genetics; trade wars; or a debt-fueled financial crisis.   
oil shortage  
a decisive event  
special collective personality  
age-bracket  
drift upward in age  
nurturing relationships  
fortyish and fiftyish, twentyish and thirtyish  
but the fortyish Silent *set the tone*  
people at the social periphery  
feel the ebb and flow of history   
The “directive” individual helps set the overall tone, the “directed” follows   
defiance of institutional norms 反抗风俗规范  
social convention 社会习俗  
, in large measure, 在很大程度上...  
To mobilize America for total war,  
played out their *respective* phase-of-life roles  
the moon landing  
an adult-perceived mission.  
schoolchildren  
social landscape  
the U.S. role in world affairs  
explain in some detail about ...  
the nation mobilized as a single organism  
an aging, principled generation  
midlifer  
the failed efforts   
a common set of moral understandings about good and bad, in the realm of individual and social action.  
frustration with public institutions  
fragmenting families and communities  
rising alcohol and drug abuse,  
a growing tendency to take risks in most spheres of life.  
Sex-role distinctions  
the protection accorded children reaches a low ebb  
to project and enforce their principles on the world around them  
steer the national mood toward pessimistic and portentous spiritualism.  
bright, well-meaning kids   
Birth-control technology  
reduced purchasing power.  
the late 1960s and 1970s.  
a society-wide hostility toward children  
consider somebody more worthy of freedom/protection  
offers much comfort to  
*speeding wagons and streetcars* were a leading cause of death among Lost city kids  
nurturing style  
Childhood nurture  
fulfill social roles  
spiritual self-discover vs teamwork  
Reactives are the most risk-prone;  
conformity  
“extroversion” (interventionism) and “introversion” (isolationism).  
“public activity” and a period of “private interest” (or “conservative retrenchment”)  
national mood: the presence or absence of public activism.  
hear these stories by word of mouth   
Near the foot of the scaffold gathered a crowd of persons of all ages.  
last-minute evidence of  
Charles I to the throne  
stop at nothing short of perfection  
showered with new schools  
moral chaos, spiritual drift   
a complete family  
exhibit zeal  
less cultivated, wilder people,   
dumb luck  
shelter kids from material harm,   
for generations *to come*  
living standards   
the young  
material abundance  
