
# magma / binar  
**magma** (or **binar**, or, rarely, **groupoid**)[^1] :  

- a set;  
- a binary operation that must be closed;     


# semigroup 
![](magma_to_group.jpg)  [^2]


# monoid 
![](magma_to_group.jpg)  [^2]


# group  
![](magma_to_group.jpg)  [^2]

# Group-like structures[^2]  
|                    | Totality | Associativity | Identity | Division / Invertibility |
|--------------------|----------|---------------|----------|--------------------------|
| Semigroupoid       |          | Y             |          |          |
| Small category     |          | Y             | Y        |          |
| [[groupoid]]       |          | Y             | Y        | Y        |
|                    |          |               |          |          |
| Magma              | Y        |               |          |          |
| Quasigroup         | Y        |               |          | Y        |
| Unital magma       | Y        |               | Y        |          |
| Loop               | Y        |               | Y        | Y        |
| Semigroup          | Y        | Y             |          |          |
| Monoid             | Y        | Y             | Y        |          |
| Group              | Y        | Y             | Y        | Y        |

(This table comes from [^2], and is trimmed, and is generated with MarkdownTableEditor[^3])

## Totality
- The [closure](https://en.wikipedia.org/wiki/Closure_(mathematics) "Closure (mathematics)") axiom, used by many sources and defined differently, is equivalent.  
- In mathematics, given set X and Y, a partial function $f : S \subset X \to Y$. If S = X, then f is said to be **total**.[^4]  




# References
[^1]: [magma in algebra](https://en.wikipedia.org/wiki/Magma_(algebra))    
[^2]: [Semigroup](https://en.wikipedia.org/wiki/Semigroup)  
[^3]: [Markdown Table Editor and Generator](https://tableconvert.com/markdown-generator), [Tables Generator](https://tablesgenerator.com/markdown_tables)  
[^4]: [Total_function](https://en.wikipedia.org/wiki/Total_function)