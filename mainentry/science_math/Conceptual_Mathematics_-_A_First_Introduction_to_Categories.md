_Conceptual Mathematics: A First Introduction to Categories_,  
(2ed, 2009)  
F.Willian Lawvere, Stephen H.Schanuel,  

##  
I like geometry, but I never found that I’m so interested in the algebra structure! This book opens a door for me to another fantastic world.

## What does this book talk about?
This is a highly reviewed book on category theory. But many concepts introduced in this book are different from the concepts in other category theory books such as ‘Category Theory in Context’ and ‘Abstract and Concrete Categories: The Joy of Cats’. So, what does this book talk about?

> On page 368, it says that ‘The goal of this book has been to show how the notion of composition of maps leads to the most natural account of the fundamental notions of mathematics, from multiplication, addition, and exponentiation, through the basic notions of logic and of connectivity. ‘

In my opinion, ‘conceptual mathematics’ may be a technical term, because there is another book ‘Mathematical Concepts’(by Jürgen Jost, 2015) on the similar topics.  

### Symbols  
S: a category of sets or maps  
C: any category  

### 
> P267. This definition works because for each s in S, either s =j1(s’) for exactly one s’ in B1 or j =j2(s”) for exactly one s” in B2, but NOT BOTH.  
> In other words, the maps j1 and j2 are injectivey and they cover the whole of S (are exhaustive) and do not overlap (are disjoint).  

In my opinion, this is the essential difference between sum and product!

### 
![](img/00/img_4766.jpg)  
![](img/00/img_4767.jpg)  
![](img/00/img_4768.jpg)  
![](img/00/img_4769.jpg)  
![](img/00/img_4770.jpg)  
![](img/00/img_4771.jpg)  
![](img/00/img_4772.jpg)  
![](img/00/img_4773.jpg)  
![](img/00/img_4774.jpg)  
![](img/00/img_4775.jpg)  
![](img/00/img_4777.jpg)  
![](img/00/img_4780.jpg)  
![](img/00/img_4781.jpg)  
![](img/00/img_4782.jpg)  
![](img/00/img_4783.jpg)  
![](img/00/img_4784.jpg)  
![](img/00/img_4785.jpg)  
![](img/00/img_4786.jpg)  
![](img/00/img_4787.jpg)  
![](img/00/img_4788.jpg)  
![](img/00/img_4789.jpg)  
![](img/00/img_4790.jpg)  
![](img/00/img_4791.jpg)  
![](img/00/img_4792.jpg)  
![](img/00/img_4794.jpg)  
![](img/00/img_4795.jpg)  
![](img/00/img_4796.jpg)  
![](img/00/img_4798.jpg)  
![](img/00/img_4799.jpg)  
![](img/00/img_4800.jpg)  
![](img/00/img_4801.jpg)  
![](img/00/img_4802.jpg)  
![](img/00/img_4803.jpg)  
![](img/00/img_4804.jpg)    
![](img/00/img_4805.jpg)  