## Fiber Bundle

## Topological Group

## Structure Group  

as b varies over the points of the base space B, we are given G-structures, one on each fiber $p^{−1}(b)$. The given G-structure on the fiber $p^{−1}(b)$ is denoted Φ(b). Thus Φ(b) is a collection of homeomorphisms from F to $p^{−1}(b)$ which satisfy conditions (a) and (b).  
(source: [structure group of a fiber bundle - StackExchange](https://math.stackexchange.com/questions/4195781/definition-of-structure-group-of-a-fiber-bundle))

## Principal Bundle  
 Let G be a topological group. A fiber bundle p : E → B with _fiber G_ and _structure group G_ is called a **principal G-bundle**, if the action of the structure group on the fiber is given by the group multiplication of G. When G is obvious from the context, it is simply called a **principal bundle.**


### Theorem 3.7.1.   
Let G be a topological group and H a subgroup. Suppose the projection p : G → G/H has local cross sections. Then fiber and structure group of (fiber bundle) p are H, the action of the structure group is given by the group multiplication μH : H × H → H of H.  

### Theorem 3.7.1.  (in term of "Principal Bundle")
Let G be a topological group and H a subgroup. If the projection p : G → G/H has local cross sections, it is a principal H-bundle.

###  Fact 3.7.7  
(a principal G bundle) + (an action of G on F ) = (a fiber bundle with structure group G and fiber F )

### Fiber-preserving Map $\bf f$=($\tilde f$, $f$) 
![](fiber-preserving-map.jpg) ([source](https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBpiBdUkANwEMAbAVxiRDQD1gBaARgF8AFAA8AlCH6l0mXPkIpe5KrUYs2AUQlT2MvASIAmJdXrNWiEACEt07HvklSvZabUW0Aci58hYmzrs5IkVnE1VzEHUPfwxA-RQjUJUzNkto-mUYKABzeCJQADMAJwgAWyQyEBwIJEVktzAmBgZqBjoAIxgGAAVdIIsGGAKcEGocOiwGNgALCAgAa39isoqxmsQAZjCUiwAdXbwGWAACAoAfAH1gTh4BEVF+UZA2zp6++OehkclCkvLEOrVJBGeoRNBPF5dXpxeSfYZLP61NZIAAs2zcIH2hxOBQhHSh71hg3hPxAy3+IKBiAArOiIrjWvi3jC2MTvtpyUgtlV1mjQUhGs1Ga9obIPmynuNJjM5otSZzEHyqbT+e5osKCSyBl8JBR+EA))    
[Latex commute diagram editor](https://tikzcd.yichuanshen.de)

