A groupoid is a set G with:  
- a unary operation ${}^{-1} : G \to G$;  
	- Associativity
	- Inverse
	- Identity
- a partial function $∗ : G \times G \to G$;   
	- Associativity
	- Inverse
	- Identity

$*$ is not necessarily [[algebra#Totality|total]](or [total](algebra.md#Totality)). ($*$ is not a binary operation because it is not necessarily defined for all pairs of elements of G)  


The algebraic and category-theoretic definitions are equivalent [^1].  




# References
[^1]: [Groupoid](https://en.wikipedia.org/wiki/Groupoid)  
